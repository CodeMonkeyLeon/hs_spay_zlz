package cn.swiftpass.enterprise.rate.interfaces

interface OnRateUpdateClickListener {
    fun onRateUpdateClick()
}