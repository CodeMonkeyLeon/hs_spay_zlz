package cn.swiftpass.enterprise.rate.view

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import android.widget.TextView
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.rate.interfaces.OnRateConfirmDialogClickListener

class RateConfirmTipsDialog : Dialog, View.OnClickListener {

    private lateinit var mContext: Context


    private lateinit var mBtnBack: TextView
    private lateinit var mBtnQuery: TextView


    private var onClickListener: OnRateConfirmDialogClickListener? = null


    constructor(
        context: Context,
        clickListener: OnRateConfirmDialogClickListener
    ) : super(context) {
        mContext = context
        onClickListener = clickListener
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_rate_confirm_tips)
        val window = window
        window!!.setBackgroundDrawable(ColorDrawable(0))
        setCanceledOnTouchOutside(false)


        mBtnBack = findViewById(R.id.id_btn_back)
        mBtnQuery = findViewById(R.id.id_btn_query)


        mBtnBack.setOnClickListener(this)
        mBtnQuery.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        view?.let { v ->
            when (v.id) {
                R.id.id_btn_back -> {
                    onClickListener?.let {
                        it.onBackClick()
                    }
                }
                R.id.id_btn_query -> {
                    onClickListener?.let {
                        it.onQueryClick()
                    }
                }
                else -> {

                }
            }
        }
    }


}