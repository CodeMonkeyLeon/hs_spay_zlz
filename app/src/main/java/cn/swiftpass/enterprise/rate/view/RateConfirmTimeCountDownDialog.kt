package cn.swiftpass.enterprise.rate.view

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.widget.TextView
import cn.swiftpass.enterprise.intl.R

class RateConfirmTimeCountDownDialog : Dialog {


    private lateinit var mContext: Context

    private lateinit var mTvText: TextView


    constructor(context: Context) : super(context) {
        mContext = context

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_rate_confirm_time_count_down)
        val window = window
        window!!.setBackgroundDrawable(ColorDrawable(0))
        setCanceledOnTouchOutside(false)

        mTvText = findViewById(R.id.id_tv_text)
    }


    fun setMessage(str: String?) {
        str?.let {
            mTvText.text = str
        }
    }

}