package cn.swiftpass.enterprise.rate.adapter

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.text.TextUtils
import android.view.View
import android.view.ViewTreeObserver
import android.widget.ImageView
import android.widget.TextView
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.rate.entity.RateItemEntity
import cn.swiftpass.enterprise.rate.interfaces.OnRateUpdateClickListener
import cn.swiftpass.enterprise.rate.view.RateConfirmActivity
import cn.swiftpass.enterprise.utils.KotlinUtils
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder

class RateAdapter(private val mContext: Context, var mList: ArrayList<RateItemEntity>) :
    BaseMultiItemQuickAdapter<RateItemEntity, BaseViewHolder>(mList) {


    private var mOnUpdateClickListener: OnRateUpdateClickListener? = null

    fun setOnRateUpdateClickListener(listener: OnRateUpdateClickListener) {
        mOnUpdateClickListener = listener
    }


    init {
        addItemType(RateItemEntity.TYPE_RATE_LOGO, R.layout.item_rate_logo)
        addItemType(RateItemEntity.TYPE_RATE_CURRENCY, R.layout.item_rate_currency)
        addItemType(RateItemEntity.TYPE_RATE_REF, R.layout.item_rate_ref)
    }


    override fun convert(helper: BaseViewHolder?, item: RateItemEntity?) {
        helper?.let { h ->
            item?.let { i ->
                when (h.itemViewType) {
                    RateItemEntity.TYPE_RATE_LOGO -> {
//                        loadLogo(h)
                    }
                    RateItemEntity.TYPE_RATE_CURRENCY -> {
                        setCurrencyLayout(h, i)
                    }
                    RateItemEntity.TYPE_RATE_REF -> {
                        setRefLayout(h, i)
                    }
                    else -> {

                    }
                }
            }
        }
    }


//    private fun loadLogo(holder: BaseViewHolder) {
//        val imgLogo = holder.itemView.findViewById<ImageView>(R.id.id_img_logo)
//        Glide.with(holder.itemView)
//            .load(R.drawable.icon_rate_confirm)
//            .into(imgLogo)
//    }


    override fun onBindViewHolder(
        holder: BaseViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            val data = payloads[0]
            if (TextUtils.equals(data.toString(), RateConfirmActivity.NO_RESULT)) {
                //更新失败
                when (position) {
                    RateConfirmActivity.INDEX_CNY -> {
                        holder.itemView.findViewById<TextView>(R.id.id_tv_amount).visibility =
                            View.GONE
                        holder.setText(
                            R.id.id_tv_currency,
                            mContext.getString(R.string.string_rate_confirm_unable_to_calculate)
                        )
                    }
                    RateConfirmActivity.INDEX_REF -> {
                        holder.setText(
                            R.id.id_tv_ref,
                            mContext.getString(R.string.string_rate_confirm_not_update)
                        )
                    }
                    else -> {

                    }
                }
            } else {
                //更新成功
                when (position) {
                    RateConfirmActivity.INDEX_CNY -> {
                        holder.itemView.findViewById<TextView>(R.id.id_tv_amount).visibility =
                            View.VISIBLE
                        holder.setText(R.id.id_tv_amount, data.toString())
                        holder.setText(
                            R.id.id_tv_currency,
                            mContext.getString(R.string.string_rate_confirm_cny)
                        )
                    }
                    RateConfirmActivity.INDEX_REF -> {
                        try {
                            val formatted =
                                KotlinUtils.retainSixDecimalPlaces.invoke(data.toString())
                            holder.setText(R.id.id_tv_ref, formatted)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    else -> {

                    }
                }
            }
        }
    }


    private fun setCurrencyLayout(helper: BaseViewHolder, item: RateItemEntity) {
        helper.setText(R.id.id_tv_title, item.title)
        if (TextUtils.isEmpty(item.amount)) {
            helper.itemView.findViewById<TextView>(R.id.id_tv_amount).visibility = View.GONE
            helper.setText(
                R.id.id_tv_currency,
                mContext.getString(R.string.string_rate_confirm_unable_to_calculate)
            )
        } else {
            helper.itemView.findViewById<TextView>(R.id.id_tv_amount).visibility = View.VISIBLE
            helper.setText(R.id.id_tv_amount, item.amount)
            helper.setText(R.id.id_tv_currency, item.currency)
        }
    }


    private fun setRefLayout(helper: BaseViewHolder, item: RateItemEntity) {
        if (!TextUtils.isEmpty(item.ref)) {
            try {
                val formatted = KotlinUtils.retainSixDecimalPlaces.invoke(item.ref)
                helper.setText(R.id.id_tv_ref, formatted)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            helper.setText(
                R.id.id_tv_ref,
                mContext.getString(R.string.string_rate_confirm_not_update)
            )
        }
        val btnUpdate = helper.itemView.findViewById<ConstraintLayout>(R.id.id_cy_rate_update)
        btnUpdate?.let { b ->
            b.setOnClickListener { v ->
                mOnUpdateClickListener?.let { listener ->
                    listener.onRateUpdateClick()
                }
            }
        }

        val tvUpdate = helper.itemView.findViewById<TextView>(R.id.id_tv_update)
        tvUpdate.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                // 在这里处理 TextView 绘制完成后的逻辑
                val imgUpdate = helper.itemView.findViewById<ImageView>(R.id.id_img_update)
                val layoutParam = imgUpdate.layoutParams as ConstraintLayout.LayoutParams
                layoutParam.height = tvUpdate.height
                layoutParam.width = tvUpdate.height
                imgUpdate.layoutParams = layoutParam
                // 在处理完成后，您可能需要移除监听器
                tvUpdate.viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        })


        val tvHelp = helper.itemView.findViewById<TextView>(R.id.id_tv_help)
        tvHelp.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                // 在这里处理 TextView 绘制完成后的逻辑
                val imgTips = helper.itemView.findViewById<ImageView>(R.id.id_img_tips)
                val layoutParam = imgTips.layoutParams as ConstraintLayout.LayoutParams
                layoutParam.height = tvHelp.height
                layoutParam.width = tvHelp.height
                imgTips.layoutParams = layoutParam
                // 在处理完成后，您可能需要移除监听器
                tvHelp.viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        })
    }
}