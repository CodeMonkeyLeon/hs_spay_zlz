package cn.swiftpass.enterprise.rate.entity

import com.chad.library.adapter.base.entity.MultiItemEntity

data class RateItemEntity(
    var type: Int = 0,
    var title: String? = "",
    var currency: String? = "",
    var amount: String? = "",
    var ref: String? = ""
) : java.io.Serializable, MultiItemEntity {


    companion object {
        const val TYPE_RATE_LOGO = 1001
        const val TYPE_RATE_CURRENCY = 1002
        const val TYPE_RATE_REF = 1003
    }


    override fun getItemType() = type
}
