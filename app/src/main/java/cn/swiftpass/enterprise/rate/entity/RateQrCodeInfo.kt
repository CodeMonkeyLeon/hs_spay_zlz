package cn.swiftpass.enterprise.rate.entity

import cn.swiftpass.enterprise.bussiness.model.WxCard

data class RateQrCodeInfo(
    val code: String? = "",
    val money: String? = "",
    val payType: String? = "",
    val vardOrders: List<WxCard>? = null,
    val discountAmount: Long = 0,
    val outTradeNo: String? = "",
    val mark: String? = "",
    var rmbExchangeRate: String? = ""
) : java.io.Serializable
