package cn.swiftpass.enterprise.rate.event

import cn.swiftpass.enterprise.partyking.event.BaseEvent

class RateConfirmEvent(var eType: Int, var eMessage: String) : BaseEvent(eType, eMessage) {
    companion object {
        const val EVENT_RATE_CONFIRM_BACK = 2391
    }
}