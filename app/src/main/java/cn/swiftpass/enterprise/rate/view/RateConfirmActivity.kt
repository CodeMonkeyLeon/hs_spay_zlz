package cn.swiftpass.enterprise.rate.view

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.bussiness.model.QRcodeInfo
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.rate.adapter.RateAdapter
import cn.swiftpass.enterprise.rate.entity.RateItemEntity
import cn.swiftpass.enterprise.rate.entity.RateQrCodeInfo
import cn.swiftpass.enterprise.rate.event.RateConfirmEvent
import cn.swiftpass.enterprise.rate.interfaces.OnRateConfirmDialogClickListener
import cn.swiftpass.enterprise.rate.interfaces.OnRateUpdateClickListener
import cn.swiftpass.enterprise.rate.manager.RateConfirmManager
import cn.swiftpass.enterprise.ui.activity.NoteMarkActivity
import cn.swiftpass.enterprise.ui.activity.PayResultActivity
import cn.swiftpass.enterprise.ui.activity.PreAutFinishActivity
import cn.swiftpass.enterprise.ui.activity.TemplateActivity
import cn.swiftpass.enterprise.ui.widget.TitleBar
import cn.swiftpass.enterprise.utils.*
import org.greenrobot.eventbus.EventBus

/**
 * @author lizheng.zhao
 * @date 2023/03/22
 * @description 汇率确认页面
 */
class RateConfirmActivity : TemplateActivity(), View.OnClickListener {
    private fun getLayoutId() = R.layout.act_rate


    private lateinit var mRecycleView: RecyclerView
    private lateinit var mAdapter: RateAdapter
    private val mItemList = ArrayList<RateItemEntity>()

    private lateinit var mBtnConfirm: Button

    private lateinit var mRateQrCodeInfo: RateQrCodeInfo

    private var mTimeCount15 = 15L
    private var mTimeCount5 = 5L
    private var mTimeCountDownDialog: RateConfirmTimeCountDownDialog? = null
    private var mStartTime = 0L
    private var mResultTime = 0L
    private var mFlag = true
    private var mOutTradeNo: String? = null
    private var mIsShowWindowReverse15 = false
    var mBackOrQueryDialog: RateConfirmTipsDialog? = null
    private val mHandler: Handler by lazy {
        Handler()
    }
    private val myRunnable15: Runnable by lazy {
        Runnable() {
            kotlin.run {
                if (mTimeCountDownDialog == null) {
                    return@Runnable
                }
                if (mTimeCount15 >= 0) {
                    if (mResultTime - mStartTime < 5000 && (mTimeCount15 == 10L || mTimeCount15 == 5L)) {
                        //5s内返回了结果，才发起下一次请求
                        queryOrderGetStatus(
                            mOutTradeNo,
                            getString(R.string.confirming_waiting),
                            false,
                            false
                        )
                    }
                    val start = getString(R.string.string_rate_confirm_remaining_1)
                    val end =
                        getString(R.string.string_rate_confirm_remaining_2) + "\n" + getString(R.string.string_rate_confirm_remaining_3)
                    val msg = "$start $mTimeCount15 $end"
                    mTimeCountDownDialog?.let { it.setMessage(msg) }
                    mTimeCount15--
                    mHandler.postDelayed(myRunnable15, 1000)
                } else {
                    mHandler.removeCallbacks(myRunnable15)
                    mTimeCountDownDialog?.let {
                        if (!isFinishing && it.isShowing) {
                            it.dismiss()
                        }
                    }
                    if (mIsShowWindowReverse15) {
                        showWindowRever()
                        mIsShowWindowReverse15 = false
                    }
                }
            }
        }
    }


    private val myRunnable5: Runnable by lazy {
        Runnable() {
            kotlin.run {
                if (mTimeCountDownDialog == null) {
                    return@Runnable
                }
                if (mTimeCount5 >= 0) {
                    val start = getString(R.string.string_rate_confirm_remaining_1)
                    val end =
                        getString(R.string.string_rate_confirm_remaining_2) + "\n" + getString(R.string.string_rate_confirm_remaining_3)
                    val msg = start + mTimeCount5 + end
                    mTimeCountDownDialog?.let { it.setMessage(msg) }
                    mTimeCount5--
                    mHandler.postDelayed(myRunnable5, 1000)
                } else {
                    mHandler.removeCallbacks(myRunnable5)
                    mTimeCountDownDialog?.let {
                        if (!isFinishing && it.isShowing) {
                            it.dismiss()
                        }
                    }
                    showWindowRever()
                }
            }
        }
    }


    companion object {
        const val TAG = "RateConfirmActivity"

        const val INDEX_CNY = 2
        const val INDEX_REF = 3

        const val NO_RESULT = "NO_RESULT"

        fun startRateConfirmActivity(fromActivity: Activity?, qrCodeInfo: RateQrCodeInfo? = null) {
            fromActivity?.let { act ->
                qrCodeInfo?.let { info ->
                    val intent = Intent(act, RateConfirmActivity::class.java)
                    intent.putExtra(Constant.RATE_CONFIRM_INFO, qrCodeInfo)
                    act.startActivity(intent)
                }

            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        intent?.let {
            mRateQrCodeInfo = it.getSerializableExtra(Constant.RATE_CONFIRM_INFO) as RateQrCodeInfo
        }
        initToolBar()
        initRecycleView()
        initView()
    }


    override fun onClick(v: View?) {
        v?.let {
            when (it.id) {
                R.id.id_btn_confirm -> {
                    if (!TextUtils.isEmpty(mRateQrCodeInfo.rmbExchangeRate)) {
                        //汇率已更新
                        OrderManager.getInstance().doCoverOrder(
                            mRateQrCodeInfo.code,
                            mRateQrCodeInfo.money,
                            mRateQrCodeInfo.payType,
                            mRateQrCodeInfo.vardOrders,
                            mRateQrCodeInfo.discountAmount,
                            mRateQrCodeInfo.outTradeNo,
                            mRateQrCodeInfo.mark,
                            true,
                            object : UINotifyListener<QRcodeInfo>() {
                                override fun onPreExecute() {
                                    super.onPreExecute()
                                    loadDialog(
                                        this@RateConfirmActivity,
                                        getString(R.string.tx_pay_loading)
                                    )
                                }

                                override fun onError(error: Any?) {
                                    super.onError(error)
                                    dismissLoading()
                                    if (checkSession()) {
                                        return
                                    }
                                    runOnUiThread {


//                                        val e = "100540000135592230702692105SPAY"
//                                        val outOrderNo: String = e.toString()
//                                            .substring(0, e.toString().length - 4)
//                                        queryOrderGetStatus(
//                                            outOrderNo,
//                                            getString(R.string.confirming_waiting),
//                                            false
//                                        )


                                        error?.let { e ->
                                            if (e.toString()
                                                    .contains("SPAY")
                                            ) {
                                                val outOrderNo = e.toString()
                                                    .substring(0, e.toString().length - 4)
                                                queryOrderGetStatus(
                                                    outOrderNo,
                                                    getString(R.string.confirming_waiting),
                                                    false
                                                )
                                            } else if (TextUtils.equals(e.toString(), "20")) {
                                                //单笔限额已经超出
                                                toastDialog(
                                                    this@RateConfirmActivity,
                                                    R.string.single_limit_exceeded
                                                ) { }
                                            } else {
                                                toastDialog(
                                                    this@RateConfirmActivity,
                                                    e.toString()
                                                ) {}
                                            }
                                        }


                                    }
                                }

                                override fun onSucceed(qRcodeInfo: QRcodeInfo?) {
                                    super.onSucceed(qRcodeInfo)
                                    qRcodeInfo?.let { result ->


//                                        val e = "100540000135592230702692105SPAY"
//                                        val outOrderNo: String = e.toString()
//                                            .substring(0, e.toString().length - 4)
//                                        queryOrderGetStatus(
//                                            outOrderNo,
//                                            getString(R.string.confirming_waiting),
//                                            false
//                                        )


                                        val orderNo =
                                            if (MainApplication.isPre_authOpen == 1 && TextUtils.equals(
                                                    PreferenceUtil.getString(
                                                        "choose_pay_or_pre_auth",
                                                        "sale"
                                                    ),
                                                    "pre_auth"
                                                )
                                            ) {
                                                result.outAuthNo
                                            } else {
                                                result.orderNo
                                            }

                                        //查询订单获取状态 定时任务查询订单状态,状态不为2 才轮询
                                        if (result.order.state != null && !TextUtils.isEmpty(result.order.state) && result.order.state != "2") {
                                            // 查询订单获取状态 定时任务查询订单状态
                                            queryOrderGetStatus(
                                                orderNo,
                                                getString(R.string.confirming_waiting),
                                                false,
                                                false
                                            )
                                        } else {
                                            //否则下单成功成功
                                            dismissLoading()
                                            result.order.money =
                                                Utils.Integer.tryParse(mRateQrCodeInfo.money, 0)
                                                    .toLong()
                                            //交易成功清除Note
                                            NoteMarkActivity.setNoteMark("")
                                            if (MainApplication.isPre_authOpen == 1 && TextUtils.equals(
                                                    PreferenceUtil.getString(
                                                        "choose_pay_or_pre_auth",
                                                        "sale"
                                                    ),
                                                    "pre_auth"
                                                )
                                            ) {
                                                PreAutFinishActivity.startActivity(
                                                    this@RateConfirmActivity,
                                                    result.order,
                                                    5
                                                )
                                            } else {
                                                //交易成功进行语音播报
                                                PayResultActivity.startActivity(
                                                    this@RateConfirmActivity,
                                                    result.order
                                                )
                                            }
                                            finish()
                                        }


                                    }
                                }
                            }
                        )
                    } else {
                        //汇率未更新，报错提示
                        toastDialog(
                            this,
                            getString(R.string.string_rate_confirm_rate_not_update),
                            getString(R.string.string_rate_confirm_ok)
                        ) {
                            Log.i(TAG, "汇率未更新")
                        }
                    }
                }
                else -> {

                }
            }
        }
    }


    private fun queryOrderGetStatus(
        orderNo: String? = "",
        title: String? = "",
        isRevers: Boolean = false,
        isShowLoading: Boolean = true,
        is5TimeCount: Boolean = false
    ) {

        Log.i(TAG, "查询接口调用 is5TimeCount: $is5TimeCount")

        mStartTime = System.currentTimeMillis()
        OrderManager.getInstance().queryOrderByOrderNo(
            orderNo,
            mRateQrCodeInfo.payType,
            object : UINotifyListener<Order>() {

                override fun onPreExecute() {
                    super.onPreExecute()
                    if (isShowLoading) {
                        loadDialog(this@RateConfirmActivity, title)
                    }
                }


                override fun onError(error: Any?) {
                    super.onError(error)
                    dismissLoading()
                    mResultTime = System.currentTimeMillis()
                    mIsShowWindowReverse15 = false
                    error?.let { e ->
                        runOnUiThread {
                            if (is5TimeCount) {
                                //5s倒计时
                                if (mTimeCount5 >= 5) {
                                    if (!(mTimeCountDownDialog != null && mTimeCountDownDialog!!.isShowing)) {
                                        mTimeCountDownDialog = RateConfirmTimeCountDownDialog(
                                            this@RateConfirmActivity
                                        )
                                        DialogHelper.resize(
                                            this@RateConfirmActivity,
                                            mTimeCountDownDialog
                                        )
                                        mTimeCountDownDialog!!.setOnKeyListener(DialogInterface.OnKeyListener { arg0, keycode, arg2 -> keycode == KeyEvent.KEYCODE_BACK })
                                        mTimeCountDownDialog!!.show()
                                        mHandler.post(myRunnable5)
                                    }
                                    mOutTradeNo = orderNo
                                } else {
                                    //1次5秒查询后，如果还未返回成功，调用冲正接口
                                    mOutTradeNo = orderNo
                                }
                            } else {
                                //15s倒计时
                                if (mTimeCount15 >= 15) {
                                    if (!(mTimeCountDownDialog != null && mTimeCountDownDialog!!.isShowing)) {
                                        mTimeCountDownDialog = RateConfirmTimeCountDownDialog(
                                            this@RateConfirmActivity
                                        )
                                        DialogHelper.resize(
                                            this@RateConfirmActivity,
                                            mTimeCountDownDialog
                                        )
                                        mTimeCountDownDialog!!.setOnKeyListener(DialogInterface.OnKeyListener { arg0, keycode, arg2 -> keycode == KeyEvent.KEYCODE_BACK })
                                        mTimeCountDownDialog!!.show()
                                        mHandler.post(myRunnable15)
                                    }
                                    mOutTradeNo = orderNo
                                } else {
                                    //1次15秒查询后，如果还未返回成功，调用冲正接口
                                    mOutTradeNo = orderNo
                                    mIsShowWindowReverse15 = true
                                }
                            }
                        }
                    }
                }


                override fun onSucceed(result: Order?) {
                    super.onSucceed(result)
                    dismissLoading()
                    mResultTime = System.currentTimeMillis()
                    mIsShowWindowReverse15 = false
                    result?.let { response ->

                        //如果交易成功则将备注清空
                        if (response.state == "2") {
                            NoteMarkActivity.setNoteMark("")
                        }
                        if (response.state == "2" && mFlag) {
                            // 支付成功
                            mFlag = false
                            dismissLoading()

                            if (MainApplication.isPre_authOpen == 1 && TextUtils.equals(
                                    PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"),
                                    "pre_auth"
                                )
                            ) {
                                PreAutFinishActivity.startActivity(
                                    this@RateConfirmActivity,
                                    result,
                                    1
                                )
                                Log.e("cxy", "B")
                            } else {
                                PayResultActivity.startActivity(this@RateConfirmActivity, result)
                            }
                            finish()
                        } else {
                            if (isRevers) {
                                if (response.state == "2") { // 支付成功
                                    if (MainApplication.isPre_authOpen == 1 && TextUtils.equals(
                                            PreferenceUtil.getString(
                                                "choose_pay_or_pre_auth",
                                                "sale"
                                            ),
                                            "pre_auth"
                                        )
                                    ) {
                                        PreAutFinishActivity.startActivity(
                                            this@RateConfirmActivity,
                                            result,
                                            1
                                        )
                                        Log.e("cxy", "C")
                                    } else {
                                        PayResultActivity.startActivity(
                                            this@RateConfirmActivity,
                                            result
                                        )
                                    }
                                    finish()
                                } else {
                                    showWindowRever()
                                }
                                return
                            }

                            if (is5TimeCount) {
                                //5s
                                if (mTimeCount5 >= 5) {
                                    if (!(mTimeCountDownDialog != null && mTimeCountDownDialog!!.isShowing)) {
                                        mTimeCountDownDialog = RateConfirmTimeCountDownDialog(
                                            this@RateConfirmActivity
                                        )
                                        DialogHelper.resize(
                                            this@RateConfirmActivity,
                                            mTimeCountDownDialog
                                        )
                                        mTimeCountDownDialog!!.setOnKeyListener { arg0, keycode, arg2 -> keycode == KeyEvent.KEYCODE_BACK }
                                        mTimeCountDownDialog!!.show()
                                        mHandler.post(myRunnable5)
                                    }
                                    mOutTradeNo = orderNo
                                } else {
                                    //1次15秒查询后，如果还未返回成功，调用冲正接口
                                    if (result.state != "2") {
                                        mOutTradeNo = orderNo
                                    }
                                }
                            } else {
                                //15s
                                if (mTimeCount15 >= 15) {
                                    if (!(mTimeCountDownDialog != null && mTimeCountDownDialog!!.isShowing)) {
                                        mTimeCountDownDialog = RateConfirmTimeCountDownDialog(
                                            this@RateConfirmActivity
                                        )
                                        DialogHelper.resize(
                                            this@RateConfirmActivity,
                                            mTimeCountDownDialog
                                        )
                                        mTimeCountDownDialog!!.setOnKeyListener { arg0, keycode, arg2 -> keycode == KeyEvent.KEYCODE_BACK }
                                        mTimeCountDownDialog!!.show()
                                        mHandler.post(myRunnable15)
                                    }
                                    mOutTradeNo = orderNo
                                } else {
                                    //1次15秒查询后，如果还未返回成功，调用冲正接口
                                    if (result.state != "2") {
                                        mOutTradeNo = orderNo
                                        mIsShowWindowReverse15 = true
                                    }
                                }
                            }
                        }
                    }
                }
            }
        )
    }


    private fun showWindowRever() {
        mBackOrQueryDialog = RateConfirmTipsDialog(
            this@RateConfirmActivity,
            object : OnRateConfirmDialogClickListener {
                override fun onBackClick() {
                    finish()
                }

                override fun onQueryClick() {
                    //继续查询
                    mTimeCount5 = 5
                    queryOrderGetStatus(
                        mOutTradeNo,
                        ToastHelper.toStr(R.string.confirming_waiting),
                        true,
                        is5TimeCount = true
                    )
                    mBackOrQueryDialog?.let {
                        it.dismiss()
                    }
                }
            }
        )
        mBackOrQueryDialog?.let {
            it.show()
            it.setOnKeyListener(DialogInterface.OnKeyListener { arg0, keycode, arg2 -> keycode == KeyEvent.KEYCODE_BACK })
        }
    }


    private fun initView() {
        mBtnConfirm = findViewById(R.id.id_btn_confirm)
        mBtnConfirm.setOnClickListener(this)
    }


    private fun initRecycleView() {
        mRecycleView = findViewById(R.id.id_rv_rate)
        val layoutManager = LinearLayoutManager(activity)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        mRecycleView.layoutManager = layoutManager
        mAdapter = RateAdapter(this, getItemList())
        mAdapter.setOnRateUpdateClickListener(object : OnRateUpdateClickListener {
            override fun onRateUpdateClick() {
                update()
            }
        })
        mRecycleView.adapter = mAdapter
    }


    private fun update() {
        RateConfirmManager.getRate(object : UINotifyListener<String>() {
            override fun onPreExecute() {
                super.onPreExecute()
                loadDialog(this@RateConfirmActivity, getString(R.string.public_data_loading))
            }


            override fun onError(error: Any?) {
                super.onError(error)
                dismissLoading()
                runOnUiThread {
                    mRateQrCodeInfo.rmbExchangeRate = ""
                    mAdapter.notifyItemChanged(INDEX_CNY, NO_RESULT)
                    mAdapter.notifyItemChanged(INDEX_REF, NO_RESULT)
                }
            }

            override fun onSucceed(result: String?) {
                super.onSucceed(result)
                dismissLoading()
                mRateQrCodeInfo.rmbExchangeRate = result
                mAdapter.notifyItemChanged(INDEX_CNY, getRealMoney())
//                mAdapter.notifyItemChanged(INDEX_REF, "0.123456789")
                mAdapter.notifyItemChanged(INDEX_REF, result)
            }
        })
    }


    private fun getItemList(): ArrayList<RateItemEntity> {
        mItemList.clear()
        mItemList.add(
            RateItemEntity(
                RateItemEntity.TYPE_RATE_LOGO
            )
        )


        val mon = if (!TextUtils.isEmpty(mRateQrCodeInfo.money)) {
            setDoubleFormat(mRateQrCodeInfo.money!!.toDouble() / 100)
        } else ""

        mItemList.add(
            RateItemEntity(
                RateItemEntity.TYPE_RATE_CURRENCY,
                getString(R.string.string_rate_confirm_transaction_amount),
                getString(R.string.string_rate_confirm_hkd),
                mon
            )
        )


        mItemList.add(
            RateItemEntity(
                RateItemEntity.TYPE_RATE_CURRENCY,
                getString(R.string.string_rate_confirm_real_paid_amount),
                getString(R.string.string_rate_confirm_cny),
                getRealMoney()
            )
        )


        mItemList.add(
            RateItemEntity(
                RateItemEntity.TYPE_RATE_REF,
                ref = mRateQrCodeInfo.rmbExchangeRate
            )
        )
        return mItemList
    }


    private fun getRealMoney(): String {
        val realM =
            if (!TextUtils.isEmpty(mRateQrCodeInfo.money)
                && !TextUtils.isEmpty(mRateQrCodeInfo.rmbExchangeRate)
            ) {
                val res =
                    mRateQrCodeInfo.money!!.toDouble() * mRateQrCodeInfo.rmbExchangeRate!!.toDouble()
                setDoubleFormat(res / 100)
            } else ""
        return realM
    }

    private fun setDoubleFormat(d: Double): String {
        return "%.2f".format(d)
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
        }
        return super.onKeyDown(keyCode, event)
    }


    override fun finish() {
        super.finish()
        EventBus.getDefault()
            .postSticky(
                RateConfirmEvent(
                    RateConfirmEvent.EVENT_RATE_CONFIRM_BACK,
                    "'rate confirm back"
                )
            )
    }


    private fun initToolBar() {
        titleBar.setLeftButtonVisible(true)
        titleBar.setRightButLayVisibleForTotal(false, "")
        titleBar.setTitle(getString(R.string.string_rate_confirm_toolbar))
        titleBar.setOnTitleBarClickListener(object : TitleBar.OnTitleBarClickListener {
            override fun onLeftButtonClick() {
                //返回
                finish()
            }

            override fun onRightButtonClick() {

            }

            override fun onRightLayClick() {

            }

            override fun onRightButLayClick() {
            }

        })
    }
}