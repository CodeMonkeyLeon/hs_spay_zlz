package cn.swiftpass.enterprise.rate.manager

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener
import cn.swiftpass.enterprise.bussiness.model.RequestResult
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.net.ApiConstant
import cn.swiftpass.enterprise.io.net.NetHelper
import cn.swiftpass.enterprise.utils.JsonUtil
import cn.swiftpass.enterprise.utils.SignUtil
import cn.swiftpass.enterprise.utils.ToastHelper
import org.json.JSONObject

object RateConfirmManager {


    private const val `PARAM_MCH_ID` = "mchId"
    private const val PARAM_SPAY_RS = "spayRs"
    private const val PARAM_NNS = "nns"


    fun getRate(listener: UINotifyListener<String>?) {

        listener?.let { l ->
            ThreadHelper.executeWithCallback(object : Executable<String>() {
                override fun execute(): String? {

                    val params = HashMap<String, Any>()
                    val json = JSONObject()

                    if (!TextUtils.isEmpty(MainApplication.merchantId)) {
                        params[`PARAM_MCH_ID`] = MainApplication.merchantId
                        json.put(`PARAM_MCH_ID`, MainApplication.merchantId)
                    }

                    val spayRs = System.currentTimeMillis().toString()
                    if (!TextUtils.isEmpty(MainApplication.getNewSignKey())) {
                        params[PARAM_SPAY_RS] = spayRs
                        json.put(PARAM_SPAY_RS, spayRs)
                        params[PARAM_NNS] = SignUtil.getInstance().createSign(
                            JsonUtil.jsonToMap(json.toString()),
                            MainApplication.getNewSignKey()
                        )
                    }

                    val url = ApiConstant.BASE_URL_PORT + "spay/mch/getDigitalCurrencyRate"
                    try {
                        val result =
                            NetHelper.httpsPost(url, JsonUtil.mapToJsons(params), spayRs, null)
                        if (!result.hasError()) {
                            val res = result.data.getString("result").toInt()
                            return if (res == 200) {
                                val json = JSONObject(result.data.getString("message"))
                                json.optString("rmbExchangeRate")
                            } else {
                                l.onError(result.data.getString("message"))
                                null
                            }
                        } else {
                            when (result.resultCode) {
                                RequestResult.RESULT_BAD_NETWORK -> {
                                    l.onError(ToastHelper.toStr(R.string.show_net_bad))
                                    l.onError(ToastHelper.toStr(R.string.show_net_timeout))
                                    l.onError(ToastHelper.toStr(R.string.show_net_server_fail))
                                }
                                RequestResult.RESULT_TIMEOUT_ERROR -> {
                                    l.onError(ToastHelper.toStr(R.string.show_net_timeout))
                                    l.onError(ToastHelper.toStr(R.string.show_net_server_fail))
                                }
                                RequestResult.RESULT_READING_ERROR -> l.onError(
                                    ToastHelper.toStr(
                                        R.string.show_net_server_fail
                                    )
                                )
                                else -> l.onError(result.data.getString("message"))
                            }
                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                        l.onError(ToastHelper.toStr(R.string.ts_fail_info))
                    }
                    return null
                }
            }, l)
        }


    }


}