package cn.swiftpass.enterprise.partyking.interfaces

import android.view.View

interface OnDialogCancelClickListener {
    fun onDialogCancelClick(view: View)
}