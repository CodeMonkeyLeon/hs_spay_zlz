package cn.swiftpass.enterprise.partyking.manager

import android.text.TextUtils
import android.util.Log
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener
import cn.swiftpass.enterprise.bussiness.model.RequestResult
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.net.ApiConstant
import cn.swiftpass.enterprise.io.net.NetHelper
import cn.swiftpass.enterprise.partyking.entity.*
import cn.swiftpass.enterprise.utils.JsonUtil
import cn.swiftpass.enterprise.utils.KotlinUtils
import cn.swiftpass.enterprise.utils.SignUtil
import cn.swiftpass.enterprise.utils.ToastHelper
import org.json.JSONArray
import org.json.JSONObject

object PtkManager {


    private const val PARAM_PACKAGE_ID = "packageId"
    private const val PARAM_SIGNATURE = "signature"
    private const val PARAM_SIGN = "sign"
    private const val PARAM_SPAY_RS = "spayRs"
    private const val PARAM_NNS = "nns"

    private const val PARAM_VOUCHER_ID = "voucherId"
    private const val PARAM_OUT_TRADE_NO = "outTradeNo"

    private const val PARAM_REDEEM_TIME = "redeemTime"
    private const val PARAM_TRADE_STATE = "tradeState"

    private const val PARAM_PAGE_NUMBER = "pageNumber"
    private const val PARAM_PAGE_SIZE = "pageSize"

    private const val PARAM_VOUCHER_ORDER_ID = "voucherOrderId"

    private const val SUCCESS_CODE = 200


    /**
     * 对参数签名
     */
    private fun signParam(json: JSONObject, hashMap: HashMap<String, String>, spayRs: Long) {
        if (!TextUtils.isEmpty(MainApplication.getSignKey())) {
            json.put(
                PARAM_SIGN,
                SignUtil.getInstance().createSign(hashMap, MainApplication.getSignKey())
            )
        }

        if (!TextUtils.isEmpty(MainApplication.getNewSignKey())) {
            json.put(PARAM_SPAY_RS, spayRs);
            json.put(
                PARAM_NNS,
                SignUtil.getInstance().createSign(
                    JsonUtil.jsonToMap(json.toString()),
                    MainApplication.getNewSignKey()
                )
            )
        }
    }


    /**
     * 处理异常返回结果
     *
     * @param isNeedPoll 是否需要轮询
     */
    private fun <T> dealWithError(
        errorCode: Int,
        listener: UINotifyListener<T>?,
        isNeedPoll: Boolean = false
    ) {
        when (errorCode) {
            //网络不可用
            RequestResult.RESULT_BAD_NETWORK -> listener?.let {
                it.onError(
                    ToastHelper.toStr(
                        R.string.show_net_bad
                    )
                )
            }
            //连接逾时
            RequestResult.RESULT_TIMEOUT_ERROR -> listener?.let {
                if (isNeedPoll) {
                    it.onError(
                        TimeOutErrorEntity(
                            RequestResult.RESULT_TIMEOUT_ERROR, ToastHelper.toStr(
                                R.string.show_net_timeout
                            )
                        )
                    )
                } else {
                    it.onError(
                        ToastHelper.toStr(
                            R.string.show_net_timeout
                        )
                    )
                }
            }
            //请求失败
            RequestResult.RESULT_READING_ERROR -> listener?.let {
                it.onError(
                    ToastHelper.toStr(
                        R.string.show_net_server_fail
                    )
                )
            }
        }
    }

    /**
     * 获取请求结果
     */
    private fun <T> getRequestResult(
        url: String,
        json: JSONObject,
        spayRs: Long,
        listener: UINotifyListener<T>?
    ): RequestResult {
        val result: RequestResult = NetHelper.httpsPost(
            url,
            json,
            spayRs.toString(),
            null
        )
        result.setNotifyListener(listener)
        return result
    }


    /**
     * 通过二维码获取ptk优惠券信息
     * @param packageId Base64解码的二维码信息中PackageID
     * @param signature Base64解码的二维码信息中PackageID的签名信息
     */
    fun getPtkVoucherInfo(
        packageId: String,
        signature: String,
        listener: UINotifyListener<VoucherInfoEntity>?
    ) {

        ThreadHelper.executeWithCallback(object : Executable<VoucherInfoEntity>() {
            override fun execute(): VoucherInfoEntity? {
                val hashMap = HashMap<String, String>()
                val json = JSONObject()

                json.put(PARAM_PACKAGE_ID, packageId)
                json.put(PARAM_SIGNATURE, signature)
                hashMap[PARAM_PACKAGE_ID] = packageId
                hashMap[PARAM_SIGNATURE] = signature


                //对参数签名
                val spayRs = System.currentTimeMillis()
                signParam(json, hashMap, spayRs)


                val url = ApiConstant.BASE_URL_PORT + "spay/partyking/voucherInfo"
                try {
                    val result = getRequestResult(
                        url,
                        json,
                        spayRs,
                        listener
                    )
                    if (result.hasError()) {
                        //失败
                        dealWithError(result.resultCode, listener)
                    } else {
                        //成功
                        val res = result.data.getString("result").toInt()
                        if (res == SUCCESS_CODE) {
                            val jsonResult = JSONObject(result.data.getString("message"))
                            Log.i("TAG_ZLZ", KotlinUtils.jsonToString(jsonResult))
                            var data = VoucherInfoEntity()
                            data.campaignName = jsonResult.optString("campaignName")
                            data.offerCurrencyType = jsonResult.optString("offerCurrencyType")
                            data.offerNetAmount = jsonResult.optString("offerNetAmount")
                            data.packageId = jsonResult.optString("packageId")
                            data.platformVoucherId = jsonResult.optString("platformVoucherId")
                            data.redeemTimeStr = jsonResult.optString("redeemTimeStr")
                            data.signature = jsonResult.optString("signature")
                            data.thiCode = jsonResult.optString("thiCode")
                            data.thiMessage = jsonResult.optString("thiMessage")
                            data.tradeState = jsonResult.optString("tradeState")
                            data.voucherDescChs = jsonResult.optString("voucherDescChs")
                            data.voucherDescCht = jsonResult.optString("voucherDescCht")
                            data.voucherDescEn = jsonResult.optString("voucherDescEn")
                            data.voucherDisplayNameChs =
                                jsonResult.optString("voucherDisplayNameChs")
                            data.voucherDisplayNameCht =
                                jsonResult.optString("voucherDisplayNameCht")
                            data.voucherDisplayNameEn = jsonResult.optString("voucherDisplayNameEn")
                            data.voucherId = jsonResult.optString("voucherId")
                            data.voucherState = jsonResult.optString("voucherState")
                            listener?.let {
                                it.onSucceed(data)
                            }
                        } else {
                            listener?.let {
                                it.onError(result.data.getString("message"))
                            }
                            return null
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    listener?.let {
                        it.onError(e.message)
                    }
                }
                return null
            }
        }, listener)
    }


    /**
     * 进行核销操作
     * @param voucherId 核销ID（优惠券信息接口返回）
     * @param signature 优惠券签名（详情接口返回）
     */
    fun doRedeem(
        voucherId: String,
        signature: String,
        listener: UINotifyListener<RedeemEntity>?
    ) {

        ThreadHelper.executeWithCallback(object : Executable<RedeemEntity>() {
            override fun execute(): RedeemEntity? {
                val hashMap = HashMap<String, String>()
                val json = JSONObject()

                json.put(PARAM_VOUCHER_ID, voucherId)
                json.put(PARAM_SIGNATURE, signature)
                hashMap[PARAM_VOUCHER_ID] = voucherId
                hashMap[PARAM_SIGNATURE] = signature

                //对参数签名
                val spayRs = System.currentTimeMillis()
                signParam(json, hashMap, spayRs)

                val url = ApiConstant.BASE_URL_PORT + "spay/partyking/redeem"
                try {
                    val result = getRequestResult(
                        url,
                        json,
                        spayRs,
                        listener
                    )
                    if (result.hasError()) {
                        //失败
                        dealWithError(result.resultCode, listener)
                    } else {
                        //成功
                        val res = result.data.getString("result").toInt()
                        if (res == SUCCESS_CODE) {
                            val jsonResult = JSONObject(result.data.getString("message"))
                            Log.i("TAG_ZLZ", KotlinUtils.jsonToString(json))
                            val data = RedeemEntity()
                            data.tradeState = jsonResult.optString("tradeState")
                            data.voucherState = jsonResult.optString("voucherState")
                            data.redeemTimeStr = jsonResult.optString("redeemTimeStr")
                            data.outTradeNo = jsonResult.optString("outTradeNo")
                            data.thiMessage = jsonResult.optString("thiMessage")
                            listener?.let {
                                it.onSucceed(data)
                            }
                        } else {
                            listener?.let {
                                it.onError(result.data.getString("message"))
                            }
                            return null
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    listener?.let {
                        it.onError(e.message)
                    }
                }
                return null
            }
        }, listener)
    }


    /**
     * 处理中查询核销状态
     *
     * @param outTradeNo 核销接口返回
     * @param voucherId 券ID
     * @param signature 优惠券签名（详情接口返回）
     */
    fun retryCheckRedeem(
        outTradeNo: String,
        voucherId: String,
        signature: String,
        listener: UINotifyListener<RedeemEntity>?
    ) {

        ThreadHelper.executeWithCallback(object : Executable<RedeemEntity>() {
            override fun execute(): RedeemEntity? {

                val hashMap = HashMap<String, String>()
                val json = JSONObject()

                json.put(PARAM_OUT_TRADE_NO, outTradeNo)
                json.put(PARAM_VOUCHER_ID, voucherId)
                json.put(PARAM_SIGNATURE, signature)
                hashMap[PARAM_OUT_TRADE_NO] = outTradeNo
                hashMap[PARAM_VOUCHER_ID] = voucherId
                hashMap[PARAM_SIGNATURE] = signature

                //对参数签名
                val spayRs = System.currentTimeMillis()
                signParam(json, hashMap, spayRs)

                val url = ApiConstant.BASE_URL_PORT + "spay/partyking/redeemQuery"
                try {

                    val result = getRequestResult(
                        url,
                        json,
                        spayRs,
                        listener
                    )

                    if (result.hasError()) {
                        //失败
                        dealWithError(result.resultCode, listener, true)
                    } else {
                        //成功
                        val res = result.data.getString("result").toInt()
                        if (res == SUCCESS_CODE) {
                            val jsonResult = JSONObject(result.data.getString("message"))
                            Log.i("TAG_ZLZ", KotlinUtils.jsonToString(json))

                            val data = RedeemEntity()
                            data.tradeState = jsonResult.optString("tradeState")
                            data.voucherState = jsonResult.optString("voucherState")
                            data.redeemTimeStr = jsonResult.optString("redeemTimeStr")
                            data.thiMessage = jsonResult.optString("thiMessage")
                            listener?.let {
                                it.onSucceed(data)
                            }
                        } else {
                            listener?.let {
                                val errorMsg = result.data.getString("message")
                                if (TextUtils.isEmpty(errorMsg)) {
                                    //超时
                                    it.onError(
                                        TimeOutErrorEntity(
                                            RequestResult.RESULT_TIMEOUT_ERROR,
                                            ToastHelper.toStr(
                                                R.string.show_net_timeout
                                            )
                                        )
                                    )
                                } else {
                                    //非超时的其他错误
                                    it.onError(result.data.getString("message"))
                                }
                            }
                            return null
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    listener?.let {
                        it.onError(e.message)
                    }
                }
                return null
            }
        }, listener)

    }


    /**
     * 获取汇总条目数量信息
     * @param time yyyyMMdd时间
     */
    fun getRedeemSummary(
        time: String,
        listener: UINotifyListener<PtkSummaryEntity>?
    ) {
        val redeemTime = time.replace("-", "")
        ThreadHelper.executeWithCallback(object : Executable<PtkSummaryEntity>() {
            override fun execute(): PtkSummaryEntity? {

                val hashMap = HashMap<String, String>()
                val json = JSONObject()

                json.put(PARAM_REDEEM_TIME, redeemTime)
                hashMap[PARAM_REDEEM_TIME] = redeemTime


                //对参数签名
                val spayRs = System.currentTimeMillis()
                signParam(json, hashMap, spayRs)

                val url = ApiConstant.BASE_URL_PORT + "spay/partyking/redeemSummary"
                try {

                    val result = getRequestResult(
                        url,
                        json,
                        spayRs,
                        listener
                    )

                    if (result.hasError()) {
                        //失败
                        dealWithError(result.resultCode, listener)
                    } else {
                        //成功
                        val res = result.data.getString("result").toInt()
                        if (res == SUCCESS_CODE) {
                            val jsonResult = JSONObject(result.data.getString("message"))
                            Log.i("TAG_ZLZ", KotlinUtils.jsonToString(json))

                            val data = PtkSummaryEntity()
                            data.totalNum = jsonResult.optString("totalNum")
                            data.successNum = jsonResult.optString("successNum")
                            data.otherNum = jsonResult.optString("otherNum")
                            listener?.let {
                                it.onSucceed(data)
                            }
                        } else {
                            listener?.let {
                                it.onError(result.data.getString("message"))
                            }
                            return null
                        }
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                    listener?.let {
                        it.onError(e.message)
                    }
                }

                return null
            }
        }, listener)
    }


    /**
     * 获取ptk列表
     * @param time yyyyMMdd格式
     * @param tradeState 状态 1-处理中 2-已核销 3-核销失败  全部的时候不传
     * @param pageNumber 页码
     * @param pageSize 每页数据条目数量
     *
     */
    fun getRedeemList(
        time: String,
        tradeState: String,
        pageNumber: Int,
        pageSize: Int,
        listener: UINotifyListener<PtkListEntity>?
    ) {
        val redeemTime = time.replace("-", "")
        ThreadHelper.executeWithCallback(object : Executable<PtkListEntity>() {
            override fun execute(): PtkListEntity? {

                val hashMap = HashMap<String, String>()
                val json = JSONObject()

                json.put(PARAM_REDEEM_TIME, redeemTime)
                json.put(PARAM_PAGE_NUMBER, pageNumber)
                json.put(PARAM_PAGE_SIZE, pageSize)
                hashMap[PARAM_REDEEM_TIME] = redeemTime
                hashMap[PARAM_PAGE_NUMBER] = pageNumber.toString()
                hashMap[PARAM_PAGE_SIZE] = pageSize.toString()

                if (!TextUtils.isEmpty(tradeState)) {
                    json.put(PARAM_TRADE_STATE, tradeState)
                    hashMap[PARAM_TRADE_STATE] = tradeState
                }

                //对参数签名
                val spayRs = System.currentTimeMillis()
                signParam(json, hashMap, spayRs)

                val url = ApiConstant.BASE_URL_PORT + "spay/partyking/redeemList"

                try {

                    val result = getRequestResult(
                        url,
                        json,
                        spayRs,
                        listener
                    )

                    if (result.hasError()) {
                        //失败
                        dealWithError(result.resultCode, listener)
                    } else {
                        //成功
                        val res = result.data.getString("result").toInt()
                        if (res == SUCCESS_CODE) {
                            val jsonResult = JSONObject(result.data.getString("message"))
                            Log.i("TAG_ZLZ", KotlinUtils.jsonToString(json))

                            val data = PtkListEntity()
                            data.`data`.clear()
                            data.currentPage = jsonResult.optString("currentPage")
                            data.pageCount = jsonResult.optString("pageCount")
                            data.perPage = jsonResult.optString("perPage")
                            data.reqFeqTime = jsonResult.optString("reqFeqTime")
                            data.totalRows = jsonResult.optString("totalRows")

                            val jsonArray: JSONArray = jsonResult.optJSONArray("data")
                            for (i in 0 until jsonArray.length()) {
                                val e = jsonArray.getJSONObject(i)
                                val item = PtkItemEntity()
                                item.campaignName = e.optString("campaignName")
                                item.offerCurrencyType = e.optString("offerCurrencyType")
                                item.offerNetAmount = e.optString("offerNetAmount")
                                item.tradeState = e.optString("tradeState")
                                item.voucherDisplayNameChs = e.optString("voucherDisplayNameChs")
                                item.voucherDisplayNameCht = e.optString("voucherDisplayNameCht")
                                item.voucherDisplayNameEn = e.optString("voucherDisplayNameEn")
                                item.voucherOrderId = e.optString("voucherOrderId")
                                item.voucherTime = e.optString("voucherTime")
                                data.`data`.add(item)
                            }
                            listener?.let {
                                it.onSucceed(data)
                            }
                        } else {
                            listener?.let {
                                it.onError(result.data.getString("message"))
                            }
                            return null
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    listener?.let {
                        it.onError(e.message)
                    }
                }
                return null
            }
        }, listener)
    }


    /**
     * 获取ptk记录详情
     * @param voucherOrderId 主键ID
     *
     */
    fun getRedeemDetail(voucherOrderId: String, listener: UINotifyListener<PtkDetailEntity>?) {
        ThreadHelper.executeWithCallback(object : Executable<PtkDetailEntity>() {
            override fun execute(): PtkDetailEntity? {

                val hashMap = HashMap<String, String>()
                val json = JSONObject()

                json.put(PARAM_VOUCHER_ORDER_ID, voucherOrderId)
                hashMap[PARAM_VOUCHER_ORDER_ID] = voucherOrderId


                //对参数签名
                val spayRs = System.currentTimeMillis()
                signParam(json, hashMap, spayRs)

                val url = ApiConstant.BASE_URL_PORT + "spay/partyking/redeemDetail"
                try {

                    val result = getRequestResult(
                        url,
                        json,
                        spayRs,
                        listener
                    )


                    if (result.hasError()) {
                        //失败
                        dealWithError(result.resultCode, listener)
                    } else {
                        //成功

                        val res = result.data.getString("result").toInt()
                        if (res == SUCCESS_CODE) {
                            val jsonResult = JSONObject(result.data.getString("message"))
                            Log.i("TAG_ZLZ", KotlinUtils.jsonToString(json))

                            val data = PtkDetailEntity()
                            data.campaignName = jsonResult.optString("campaignName")
                            data.offerCurrencyType = jsonResult.optString("offerCurrencyType")
                            data.offerNetAmount = jsonResult.optString("offerNetAmount")
                            data.outTradeNo = jsonResult.optString("outTradeNo")
                            data.packageId = jsonResult.optString("packageId")
                            data.redeemTimeStr = jsonResult.optString("redeemTimeStr")
                            data.thiMessage = jsonResult.optString("thiMessage")
                            data.tradeState = jsonResult.optString("tradeState")
                            data.voucherDescChs = jsonResult.optString("voucherDescChs")
                            data.voucherDescCht = jsonResult.optString("voucherDescCht")
                            data.voucherDescEn = jsonResult.optString("voucherDescEn")
                            data.voucherDisplayNameChs =
                                jsonResult.optString("voucherDisplayNameChs")
                            data.voucherDisplayNameCht =
                                jsonResult.optString("voucherDisplayNameCht")
                            data.voucherDisplayNameEn = jsonResult.optString("voucherDisplayNameEn")
                            data.voucherId = jsonResult.optString("voucherId")
                            data.voucherOrderId = jsonResult.optString("voucherOrderId")
                            data.signature = jsonResult.optString("signature")

                            listener?.let {
                                it.onSucceed(data)
                            }
                        } else {
                            listener?.let {
                                it.onError(result.data.getString("message"))
                            }
                            return null
                        }

                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    listener?.let {
                        it.onError(e.message)
                    }
                }
                return null
            }
        }, listener)
    }


}