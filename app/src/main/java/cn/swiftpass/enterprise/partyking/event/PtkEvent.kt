package cn.swiftpass.enterprise.partyking.event

class PtkEvent(var eType: Int, var eMessage: String) : BaseEvent(eType, eMessage) {
    companion object {
        /**
         * EVENT_UPDATE_PTK_SUMMARY_LIST     刷新ptk汇总数据
         * EVENT_STOP_SCAN_CAPTURE               点击finish按钮返回键盘界面
         */
        const val EVENT_UPDATE_PTK_SUMMARY_LIST = 1000
        const val EVENT_STOP_SCAN_CAPTURE = 1001
    }
}