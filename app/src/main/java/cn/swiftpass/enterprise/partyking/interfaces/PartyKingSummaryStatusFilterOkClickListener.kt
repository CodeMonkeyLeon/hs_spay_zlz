package cn.swiftpass.enterprise.partyking.interfaces

interface OnPartyKingSummaryStatusFilterOkClickListener {
    fun onPartyKingSummaryStatusFilterOkClick(chooseType: String)
}