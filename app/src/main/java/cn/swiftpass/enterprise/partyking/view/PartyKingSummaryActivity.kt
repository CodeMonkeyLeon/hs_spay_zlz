package cn.swiftpass.enterprise.partyking.view

import android.app.Activity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.partyking.adapter.PartyKingSummaryAdapter
import cn.swiftpass.enterprise.partyking.entity.*
import cn.swiftpass.enterprise.partyking.event.PtkEvent
import cn.swiftpass.enterprise.partyking.interfaces.OnPartyKingSummaryStatusFilterOkClickListener
import cn.swiftpass.enterprise.partyking.interfaces.OnPtkSummaryItemClickListener
import cn.swiftpass.enterprise.partyking.manager.PtkManager
import cn.swiftpass.enterprise.partyking.utils.PtkUtils
import cn.swiftpass.enterprise.partyking.view.PartyKingSummaryStatusFilterPopupWindow.Companion.TYPE_REDEEM_STATE_ALL
import cn.swiftpass.enterprise.partyking.view.PartyKingSummaryStatusFilterPopupWindow.Companion.TYPE_REDEEM_STATE_FAIL
import cn.swiftpass.enterprise.partyking.view.PartyKingSummaryStatusFilterPopupWindow.Companion.TYPE_REDEEM_STATE_PROCESSING
import cn.swiftpass.enterprise.partyking.view.PartyKingSummaryStatusFilterPopupWindow.Companion.TYPE_REDEEM_STATE_SUCCESS
import cn.swiftpass.enterprise.partyking.view.refresh.PtkRefreshFooter
import cn.swiftpass.enterprise.partyking.view.refresh.PtkRefreshHeader
import cn.swiftpass.enterprise.ui.activity.TemplateActivity
import cn.swiftpass.enterprise.ui.widget.SelectDatePopupWindow
import cn.swiftpass.enterprise.ui.widget.TitleBar
import cn.swiftpass.enterprise.utils.ActivitySkipUtil
import cn.swiftpass.enterprise.utils.DateUtil
import cn.swiftpass.enterprise.utils.KotlinUtils
import cn.swiftpass.enterprise.utils.Logger
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class PartyKingSummaryActivity : TemplateActivity(), View.OnClickListener {

    private fun getLayoutId() = R.layout.act_ptk_summary

    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter: PartyKingSummaryAdapter
    private val mList = mutableListOf<PtkItemEntity>()
    private lateinit var mLinearLayoutDateChoose: LinearLayout
    private lateinit var mLinearLayoutStatusChoose: LinearLayout
    private lateinit var mSmartRefreshLayout: SmartRefreshLayout
    private lateinit var mPtkRefreshHeader: PtkRefreshHeader
    private lateinit var mPtkRefreshFooter: PtkRefreshFooter


    private lateinit var mTvTime: TextView
    private lateinit var mTvTopTime: TextView
    private var mTime: String = ""
    private var mTradeState: String = ""
    private var mSelectDatePopupWindow: SelectDatePopupWindow? = null
    private var mSelectStatusPopupWindow: PartyKingSummaryStatusFilterPopupWindow? = null

    private lateinit var mTvTotal: TextView
    private lateinit var mTvSucceed: TextView
    private lateinit var mTvOther: TextView

    private lateinit var mTvStatus: TextView

    private lateinit var mClNoRecord: ConstraintLayout


    private var mPageNumber = 1


    companion object {
        const val TAG = "PtkSummaryActivity"

        private const val TEXT_SIZE_16 = 16f
        private const val TEXT_SIZE_14 = 14f

        private const val PAGE_SIZE = 10

        private const val FIRST_PAGE = "1"


        fun startPtkSummaryActivity(fromActivity: Activity) {
            ActivitySkipUtil.startAnotherActivity(
                fromActivity,
                PartyKingSummaryActivity::class.java
            )
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        EventBus.getDefault().register(this)
        mList.clear()
        initView()
        initData()
    }


    private fun initData() {
        val currentTime = System.currentTimeMillis()
        mTime = DateUtil.formatYYMD(currentTime)
        mTradeState = ""
        loadData()
    }


    /**
     * 加载数据
     */
    private fun loadData() {
        getSummaryRecordItemNumberData(mTime)
    }


    /**
     * 获取汇总记录数目信息
     */
    private fun getSummaryRecordItemNumberData(redeemTime: String) {
        PtkManager.getRedeemSummary(
            redeemTime,
            object : UINotifyListener<PtkSummaryEntity>() {

                override fun onPreExecute() {
                    super.onPreExecute()
                    loadDialog(activity, getString(R.string.loading))
                }

                override fun onError(error: Any?) {
                    super.onError(error)
                    dismissLoading()
                    if (checkSession()) {
                        return
                    }
                    runOnUiThread {
                        mSmartRefreshLayout.finishRefresh()
                        error?.let { e ->
                            toastDialog(activity, e.toString(), null)
                        }
                    }
                }

                override fun onSucceed(result: PtkSummaryEntity?) {
                    super.onSucceed(result)
                    result?.let { res ->
                        mPageNumber = 1
                        getSummaryRecordList(mPageNumber)

                        runOnUiThread {
                            mTvTotal.text = res.totalNum
                            mTvSucceed.text = res.successNum
                            mTvOther.text = res.otherNum
                        }
                    }
                }

            })
    }


    /**
     * 获取汇总记录
     */
    private fun getSummaryRecordList(pageNumber: Int) {
        PtkManager.getRedeemList(
            mTime,
            mTradeState,
            pageNumber,
            PAGE_SIZE,
            object : UINotifyListener<PtkListEntity>() {

                override fun onPreExecute() {
                    super.onPreExecute()
                    loadDialog(activity, getString(R.string.loading))
                }

                override fun onError(error: Any?) {
                    super.onError(error)
                    dismissLoading()
                    if (checkSession()) {
                        return
                    }
                    runOnUiThread {
                        mSmartRefreshLayout.finishRefresh()
                        error?.let { e ->
                            toastDialog(activity, e.toString(), null)
                        }
                    }
                }


                override fun onSucceed(result: PtkListEntity?) {
                    super.onSucceed(result)
                    dismissLoading()
                    result?.let { res ->
                        runOnUiThread {
                            val isNoData = res.data.size <= 0
                            if (TextUtils.equals(res.currentPage, FIRST_PAGE)) {
                                //首页
                                mSmartRefreshLayout.finishRefresh()
                                showNoDataLayout(isNoData)
                                if (!isNoData) {
                                    //有数据
                                    mList.clear()
                                    mList.addAll(res.data)
                                    mAdapter.notifyDataSetChanged()
                                }
                            } else {
                                //加载页
                                mSmartRefreshLayout.finishLoadMore()
                                if (!isNoData) {
                                    //有数据
                                    mList.addAll(duplicateRemove(res.data))
                                    mAdapter.notifyDataSetChanged()
                                }
                            }
                        }
                    }
                }
            })
    }


    /**
     * 去重操作
     *
     * 两个手机登录同一个账号，一个手机扫ptk码，另一个手机在汇总界面下拉加载
     * 需要进行去重操作
     */
    private fun duplicateRemove(list: ArrayList<PtkItemEntity>): ArrayList<PtkItemEntity> {
        try {
            val same = mutableListOf<Int>()
            same.clear()
            mList.forEach { itemOld ->
                list.forEachIndexed { i, itemNew ->
                    if (TextUtils.equals(itemNew.voucherOrderId, itemOld.voucherOrderId)) {
                        same.add(i)
                    }
                }
            }
            same.forEach { item ->
                list.removeAt(item)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return list
    }


    private fun setTvTime(time: String, textSize: Float) {
        mTvTime.text = time
        mTvTime.textSize = textSize
        mTvTopTime.text = time
    }

    private fun initView() {
        mRecyclerView = findViewById(R.id.id_recycle_view_ptk_summary)
        mSmartRefreshLayout = findViewById(R.id.id_ptk_summary_smart_refresh)
        mPtkRefreshHeader = findViewById(R.id.id_ptk_summary_refresh_header)
        mPtkRefreshFooter = findViewById(R.id.id_ptk_summary_refresh_footer)
        mLinearLayoutDateChoose = findViewById(R.id.ll_summary_choice_date)
        mLinearLayoutStatusChoose = findViewById(R.id.ll_summary_choice_status)
        mTvTime = findViewById(R.id.tv_time)
        mTvTopTime = findViewById(R.id.tv_pk_summary_top_time)
        mTvTotal = findViewById(R.id.tv_pk_summary_top_total_num)
        mTvSucceed = findViewById(R.id.tv_pk_summary_top_succeed_num)
        mTvOther = findViewById(R.id.tv_pk_summary_top_other_num)
        mTvStatus = findViewById(R.id.tv_status)
        mClNoRecord = findViewById(R.id.id_cl_no_record)


        mLinearLayoutDateChoose.setOnClickListener(this)
        mLinearLayoutStatusChoose.setOnClickListener(this)

        initRecyclerView()
        initRefreshLayout()
    }


    private fun showNoDataLayout(isNoData: Boolean) {
        if (isNoData) {
            mClNoRecord.visibility = View.VISIBLE
            mRecyclerView.visibility = View.GONE
        } else {
            mClNoRecord.visibility = View.GONE
            mRecyclerView.visibility = View.VISIBLE
        }
    }


    /**
     * 初始化 RecyclerView
     */
    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(activity)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        mRecyclerView.layoutManager = layoutManager

        mAdapter = PartyKingSummaryAdapter(activity, mList)
        mAdapter.setOnPtkSummaryItemClickListener(object : OnPtkSummaryItemClickListener {
            override fun onPtkSummaryItemClick(
                view: View?,
                position: Int,
                item: PtkItemEntity
            ) {
                Logger.d(TAG, "---position : $position 点击了 ptk summary item")
                getDetailsData(item)
            }
        })
        mRecyclerView.adapter = mAdapter
    }


    /**
     * 获取记录详情信息
     */
    private fun getDetailsData(data: PtkItemEntity) {

        PtkManager.getRedeemDetail(
            data.voucherOrderId,
            object : UINotifyListener<PtkDetailEntity>() {


                override fun onPreExecute() {
                    super.onPreExecute()
                    loadDialog(activity, getString(R.string.loading))
                }

                override fun onError(error: Any?) {
                    super.onError(error)
                    dismissLoading()
                    if (checkSession()) {
                        return
                    }
                    runOnUiThread {
                        error?.let { e ->
                            toastDialog(activity, e.toString(), null)
                        }
                    }
                }

                override fun onSucceed(result: PtkDetailEntity?) {
                    super.onSucceed(result)
                    result?.let { res ->
                        goToDetailPage(res)
                    }
                }
            })
    }


    private fun changeDataType(data: PtkDetailEntity): VoucherInfoEntity {
        val voucherInfo = VoucherInfoEntity()
        voucherInfo.campaignName = data.campaignName
        voucherInfo.offerCurrencyType = data.offerCurrencyType
        voucherInfo.offerNetAmount = data.offerNetAmount
        voucherInfo.packageId = data.packageId
//        temp.platformVoucherId=data.platformVoucherId
        voucherInfo.redeemTimeStr = data.redeemTimeStr
        voucherInfo.signature = data.signature
//        temp.thiCode=data.thiCode
        voucherInfo.thiMessage = data.thiMessage
        voucherInfo.tradeState = data.tradeState
        voucherInfo.voucherDescChs = data.voucherDescChs
        voucherInfo.voucherDescCht = data.voucherDescCht
        voucherInfo.voucherDescEn = data.voucherDescEn
        voucherInfo.voucherDisplayNameChs = data.voucherDisplayNameChs
        voucherInfo.voucherDisplayNameCht = data.voucherDisplayNameCht
        voucherInfo.voucherDisplayNameEn = data.voucherDisplayNameEn
        voucherInfo.voucherId = data.voucherId
//        temp.voucherState=data.voucherState
        return voucherInfo
    }


    private fun goToDetailPage(data: PtkDetailEntity) {
        val voucherInfo = changeDataType(data)

        when (data.tradeState) {
            PtkItemEntity.STATUS_TYPE_SUCCEED -> {
                PartyKingDetailsActivity.startPtkDetailsActivity(
                    activity,
                    PtkUtils.getWriteOffSuccessRecordData(activity, voucherInfo),
                    VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_RECORD_SUCCESS,
                    getString(R.string.string_ptk_voucher)
                )
            }
            PtkItemEntity.STATUS_TYPE_FAILED -> {
                PartyKingDetailsActivity.startPtkDetailsActivity(
                    activity,
                    PtkUtils.getWriteOffFailedRecordData(activity, voucherInfo),
                    VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_RECORD_FAIL,
                    getString(R.string.string_ptk_voucher)
                )
            }
            PtkItemEntity.STATUS_TYPE_PROCESSING -> {
                PartyKingDetailsActivity.startPtkDetailsActivity(
                    activity,
                    PtkUtils.getWriteOffProcessingRecordData(activity, voucherInfo),
                    VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_RECORD_PROCESSING,
                    getString(R.string.string_ptk_voucher),
                    voucherInfo,
                    data.outTradeNo
                )
            }
            else -> {}
        }
    }


    /**
     * 初始化刷新布局
     */
    private fun initRefreshLayout() {
        mPtkRefreshFooter.setLoadText(
            getString(R.string.pull_to_refresh_no_more),
            getString(R.string.pull_to_refresh_no_more),
            getString(R.string.pull_to_refresh_no_more),
        )

        mPtkRefreshHeader.setRefreshText(
            getString(R.string.drop_dowm),
            getString(R.string.release_update),
            getString(R.string.loading)
        )

//        mPtkRefreshFooter.setLoadText(
//            getString()
//        )


        mSmartRefreshLayout.setOnRefreshLoadMoreListener(object : OnRefreshLoadMoreListener {
            override fun onRefresh(refreshLayout: RefreshLayout) {
                //下拉刷新
                loadData()
            }

            override fun onLoadMore(refreshLayout: RefreshLayout) {
                //上拉加载
                mPageNumber++
                getSummaryRecordList(mPageNumber)
            }
        })
    }


    override fun onClick(v: View?) {
        v?.let { view ->
            when (view.id) {
                R.id.ll_summary_choice_date -> {
                    //时间筛选
                    Logger.d(TAG, "--- 点击了 summary date")
                    clickSelectData(mLinearLayoutDateChoose)
                }
                R.id.ll_summary_choice_status -> {
                    //状态筛选
                    Logger.d(TAG, "--- 点击了 summary status")
                    clickSelectStatus(mLinearLayoutStatusChoose)
                }
            }
        }
    }


    /**
     * 点击状态, 进行选择
     */
    private fun clickSelectStatus(view: View) {
        mSelectStatusPopupWindow =
            PartyKingSummaryStatusFilterPopupWindow(activity, mTradeState, object :
                OnPartyKingSummaryStatusFilterOkClickListener {
                override fun onPartyKingSummaryStatusFilterOkClick(chooseType: String) {
                    Logger.d(TAG, "$chooseType")
                    mTradeState = chooseType
                    when (chooseType) {
                        TYPE_REDEEM_STATE_PROCESSING -> mTvStatus.text =
                            getString(R.string.string_ptk_redeem_processing)
                        TYPE_REDEEM_STATE_SUCCESS -> mTvStatus.text =
                            getString(R.string.string_ptk_redeem_succeed)
                        TYPE_REDEEM_STATE_FAIL -> mTvStatus.text =
                            getString(R.string.string_ptk_redeem_failed)
                        TYPE_REDEEM_STATE_ALL -> mTvStatus.text =
                            getString(R.string.string_ptk_all_status)
                    }
                    loadData()
                }
            })
        mSelectStatusPopupWindow?.let {
            it.showAtLocation(
                view,
                Gravity.BOTTOM or Gravity.CENTER_HORIZONTAL,
                0,
                0
            )
        }
    }


    /**
     * 点击时间 进行选择
     */
    private fun clickSelectData(view: View) {
        mSelectDatePopupWindow =
            SelectDatePopupWindow(activity, false, mTime) { time ->
                if (time == 0L) {
                    if (KotlinUtils.isToday(mTime)) {
                        //日期为今天
                        setTvTime(getString(R.string.tx_today), TEXT_SIZE_16)
                    } else {
                        //日期不是今天
                        setTvTime(mTime, TEXT_SIZE_16)
                    }
                } else {
                    val nowTime = System.currentTimeMillis()
                    if (time > nowTime) {
                        //选择的日期不能超过今天
                        toastDialog(activity, R.string.tx_date, null)
                        return@SelectDatePopupWindow
                    }

//                    val calendar = Calendar.getInstance()
//                    calendar.add(Calendar.DAY_OF_MONTH, -90)
//
//                    if (time <= calendar.timeInMillis) {
//                        //选择时间不能超过90天
//                        toastDialog(activity, R.string.tx_choice_date, null)
//                        return@SelectDatePopupWindow
//                    }
                    setTime(time)
                    mTime = DateUtil.formatYYMD(time)
                }
                mSelectDatePopupWindow?.let {
                    it.dismiss()
                }
                loadData()
            }

        mSelectDatePopupWindow?.let {
            it.showAtLocation(
                view,
                Gravity.BOTTOM or Gravity.CENTER_HORIZONTAL,
                0,
                0
            )
        }
    }


    /**
     * 设置条目栏中的时间显示内容
     */
    private fun setTime(time: Long) {
        val newDate = DateUtil.formatYYMD(System.currentTimeMillis())
        val date = DateUtil.formatYYMD(time)
        val arr1 = newDate.split("-").toTypedArray()
        val arr2 = date.split("-").toTypedArray()
        if (arr1[0] == arr2[0] && arr1[1] == arr2[1] && arr1[2] == arr2[2]) {
            setTvTime(getString(R.string.tx_today), TEXT_SIZE_16)
        } else {
            setTvTime(DateUtil.formatYYMD(time), TEXT_SIZE_14)
        }
    }


    override fun setupTitleBar() {
        super.setupTitleBar()
        titleBar.setLeftButtonVisible(true)
        titleBar.setRightButLayVisible(false, "")
        titleBar.setTitle(getString(R.string.string_ptk_summary))
        titleBar.setOnTitleBarClickListener(object : TitleBar.OnTitleBarClickListener {
            override fun onLeftButtonClick() {
                //返回
                finish()
            }

            override fun onRightButtonClick() {

            }

            override fun onRightLayClick() {

            }

            override fun onRightButLayClick() {

            }

        })
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: PtkEvent?) {
        event?.let {
            if (it.eType == PtkEvent.EVENT_UPDATE_PTK_SUMMARY_LIST) {
                loadData()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }
}