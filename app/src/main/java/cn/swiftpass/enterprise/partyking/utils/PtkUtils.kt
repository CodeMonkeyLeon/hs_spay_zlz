package cn.swiftpass.enterprise.partyking.utils

import android.content.Context
import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.partyking.entity.PtkItemEntity
import cn.swiftpass.enterprise.partyking.entity.VoucherInfoEntity
import cn.swiftpass.enterprise.partyking.entity.VoucherInfoItemEntity
import cn.swiftpass.enterprise.utils.KotlinUtils

object PtkUtils {


    /**
     * 优惠券描述 - 多语言
     */
    private fun getDescriptionByLanguageType(
        infoEntity: VoucherInfoEntity
    ): String {
        return when (KotlinUtils.getLanguageType()) {
            //简体
            MainApplication.LANG_CODE_ZH_CN -> infoEntity.voucherDescCht
            //繁体
            MainApplication.LANG_CODE_ZH_TW -> infoEntity.voucherDescCht
            //默认英文
            else -> infoEntity.voucherDescEn
        }
    }


    /**
     * 优惠券名称 - 多语言
     */
    private fun getVoucherNameByLanguageType(
        infoEntity: VoucherInfoEntity
    ): String {
        return when (KotlinUtils.getLanguageType()) {
            //简体
            MainApplication.LANG_CODE_ZH_CN -> infoEntity.voucherDisplayNameCht
            //繁体
            MainApplication.LANG_CODE_ZH_TW -> infoEntity.voucherDisplayNameCht
            //默认英文
            else -> infoEntity.voucherDisplayNameEn
        }
    }


    /**
     * 优惠券名称 - 多语言
     */
    fun getVoucherNameByLanguageTypePtkSummary(
        infoEntity: PtkItemEntity
    ): String {
        return when (KotlinUtils.getLanguageType()) {
            //简体
            MainApplication.LANG_CODE_ZH_CN -> infoEntity.voucherDisplayNameCht
            //繁体
            MainApplication.LANG_CODE_ZH_TW -> infoEntity.voucherDisplayNameCht
            //默认英文
            else -> infoEntity.voucherDisplayNameEn
        }
    }

    /**
     * 核销确认 : ACTIVE
     */
    fun getWriteOffConfirmData(
        context: Context?,
        data: VoucherInfoEntity
    ): ArrayList<VoucherInfoItemEntity> {
        val list = ArrayList<VoucherInfoItemEntity>()
        list.clear()


        context?.let {
            val itemV1 = VoucherInfoItemEntity(
                type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                midTitle = it.getString(R.string.string_ptk_offer_name),
                midContent = getVoucherNameByLanguageType(data)
            )


            val itemV2 = VoucherInfoItemEntity(
                type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                midTitle = it.getString(R.string.string_ptk_voucher_id),
                midContent = data.voucherId
            )

            val itemH = VoucherInfoItemEntity(
                type = VoucherInfoItemEntity.ITEM_TYPE_MID_HORIZONTAL,
                midTitle = it.getString(R.string.string_ptk_voucher_status),
                midContent = data.voucherState,
                midContentHorizontalColor = VoucherInfoItemEntity.COLOR_MID_CONTENT_SUCCESS
            )

            val itemV3 = VoucherInfoItemEntity(
                type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                midTitle = it.getString(R.string.string_ptk_voucher_value),
                midContent = "${MainApplication.feeFh} ${data.offerNetAmount}"
            )

            val itemV4 = VoucherInfoItemEntity(
                type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                midTitle = it.getString(R.string.string_ptk_voucher_description),
                midContent = getDescriptionByLanguageType(data)
            )

            val itemV5 = VoucherInfoItemEntity(
                type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                midTitle = it.getString(R.string.string_ptk_package_id),
                midContent = data.packageId
            )

            val itemB = VoucherInfoItemEntity(
                type = VoucherInfoItemEntity.ITEM_TYPE_BOTTOM,
                bottomType = VoucherInfoItemEntity.BOTTOM_BLUE_BUTTON,
                bottomButtonText = it.getString(R.string.string_ptk_redeem),
                pageType = VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_CONFIRM
            )

            list.add(itemV1)
            list.add(itemV2)
            list.add(itemH)
            list.add(itemV3)
            list.add(itemV4)
            list.add(itemV5)
            list.add(itemB)
        }
        return list
    }


    /**
     * 获取不可核销的title
     */
    private fun getCanNotWriteOffTitle(data: VoucherInfoEntity, context: Context): String {
        return if (TextUtils.equals(data.voucherState, VoucherInfoEntity.VOUCHER_STATE_INACTIVE)) {
            context.getString(R.string.string_ptk_not_available)
        } else {
            context.getString(R.string.string_ptk_cannot_redeem_again)
        }
    }


    /**
     * 不可核销 : INACTIVE 、REDEEMED
     */
    fun getCanNotWriteOffConfirmData(
        context: Context?,
        data: VoucherInfoEntity
    ): ArrayList<VoucherInfoItemEntity> {
        val list = ArrayList<VoucherInfoItemEntity>()
        list.clear()
        context?.let {
            val itemTop = VoucherInfoItemEntity(
                type = VoucherInfoItemEntity.ITEM_TYPE_TOP,
                topTitle = getCanNotWriteOffTitle(data, it),
                topIconType = VoucherInfoItemEntity.TOP_ICON_FAIL
            )


            val itemV1 = VoucherInfoItemEntity(
                type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                midTitle = it.getString(R.string.string_ptk_offer_name),
                midContent = getVoucherNameByLanguageType(data)
            )

            val itemV2 = VoucherInfoItemEntity(
                type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                midTitle = it.getString(R.string.string_ptk_voucher_id),
                midContent = data.voucherId
            )

//            val itemH = VoucherInfoItemEntity(
//                type = VoucherInfoItemEntity.ITEM_TYPE_MID_HORIZONTAL,
//                midTitle = it.getString(R.string.string_ptk_voucher_status),
//                midContent = data.voucherState,
//                midContentHorizontalColor = VoucherInfoItemEntity.COLOR_MID_CONTENT_FAIL
//            )

            val itemV3 = VoucherInfoItemEntity(
                type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                midTitle = it.getString(R.string.string_ptk_voucher_value),
                midContent = "${MainApplication.feeFh} ${data.offerNetAmount}"
            )

            val itemV4 = VoucherInfoItemEntity(
                type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                midTitle = it.getString(R.string.string_ptk_voucher_description),
                midContent = getDescriptionByLanguageType(data)
            )

            val itemV5 = VoucherInfoItemEntity(
                type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                midTitle = it.getString(R.string.string_ptk_package_id),
                midContent = data.packageId
            )

            val itemB = VoucherInfoItemEntity(
                type = VoucherInfoItemEntity.ITEM_TYPE_BOTTOM,
                bottomType = VoucherInfoItemEntity.BOTTOM_BLUE_BUTTON,
                bottomButtonText = it.getString(R.string.string_ptk_redeem),
                pageType = VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_CAN_NOT_WRITE_OFF
            )

            list.add(itemTop)
            list.add(itemV1)
            list.add(itemV2)
//            list.add(itemH)
            list.add(itemV3)
            list.add(itemV4)
            list.add(itemV5)
            list.add(itemB)
        }
        return list
    }


    /**
     * 核销成功
     */
    fun getWriteOffSuccessData(
        context: Context?,
        data: VoucherInfoEntity?,
        redeemTime: String
    ): ArrayList<VoucherInfoItemEntity> {
        val list = ArrayList<VoucherInfoItemEntity>()
        list.clear()
        context?.let { con ->
            data?.let { da ->
                val itemTop = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_TOP,
                    topTitle = con.getString(R.string.string_ptk_redeem_succeed),
                    topIconType = VoucherInfoItemEntity.TOP_ICON_SUCCESS
                )


                val itemV1 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_offer_name),
                    midContent = getVoucherNameByLanguageType(da)
                )

                val itemV2 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_id),
                    midContent = da.voucherId
                )

                val itemV3 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_value),
                    midContent = "${MainApplication.feeFh} ${da.offerNetAmount}"
                )

                val itemV4 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_description),
                    midContent = getDescriptionByLanguageType(da)
                )

                val itemV5 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_package_id),
                    midContent = da.packageId
                )

                val itemV6 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_redeem_time),
                    midContent = redeemTime
                )

                val itemB = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_BOTTOM,
                    bottomType = VoucherInfoItemEntity.BOTTOM_WHITE_BUTTON_AND_GUIDE,
                    bottomGuideText = con.getString(R.string.string_ptk_more_record),
                    bottomButtonText = con.getString(R.string.string_ptk_all_finish),
                    pageType = VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_SUCCESS
                )

                list.add(itemTop)
                list.add(itemV1)
                list.add(itemV2)
                list.add(itemV3)
                list.add(itemV4)
                list.add(itemV5)
                list.add(itemV6)
                list.add(itemB)
            }
        }
        return list
    }


    /**
     * 核销进行中
     */
    fun getWriteOffProcessingData(
        context: Context?,
        data: VoucherInfoEntity?
    ): ArrayList<VoucherInfoItemEntity> {
        val list = ArrayList<VoucherInfoItemEntity>()
        list.clear()
        context?.let { con ->
            data?.let { da ->

                val itemTop = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_TOP,
                    topTitle = con.getString(R.string.string_ptk_redeem_processing),
                    topIconType = VoucherInfoItemEntity.TOP_ICON_PROCESSING
                )


                val itemV1 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_offer_name),
                    midContent = getVoucherNameByLanguageType(da)
                )

                val itemV2 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_id),
                    midContent = da.voucherId
                )

                val itemV3 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_value),
                    midContent = "${MainApplication.feeFh} ${da.offerNetAmount}"
                )

                val itemV4 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_description),
                    midContent = getDescriptionByLanguageType(da)
                )

                val itemV5 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_package_id),
                    midContent = da.packageId
                )

                val itemB = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_BOTTOM,
                    bottomType = VoucherInfoItemEntity.BOTTOM_BLUE_BUTTON_AND_GUIDE,
                    bottomGuideText = con.getString(R.string.string_ptk_more_record),
                    bottomButtonText = con.getString(R.string.string_ptk_retry),
                    pageType = VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_PROCESSING
                )

                list.add(itemTop)
                list.add(itemV1)
                list.add(itemV2)
                list.add(itemV3)
                list.add(itemV4)
                list.add(itemV5)
                list.add(itemB)
            }
        }
        return list
    }


    /**
     * 核销失败
     */
    fun getWriteOffFailedData(
        context: Context?,
        data: VoucherInfoEntity?,
        errorMsg: String
    ): ArrayList<VoucherInfoItemEntity> {
        val list = ArrayList<VoucherInfoItemEntity>()
        list.clear()
        context?.let { con ->
            data?.let { da ->

                val itemTop = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_TOP,
                    topTitle = con.getString(R.string.string_ptk_redeem_failed),
                    topIconType = VoucherInfoItemEntity.TOP_ICON_FAIL
                )


                val itemV1 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_offer_name),
                    midContent = getVoucherNameByLanguageType(da)
                )

                val itemV2 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_id),
                    midContent = da.voucherId
                )

                val itemV3 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_value),
                    midContent = "${MainApplication.feeFh} ${da.offerNetAmount}"
                )

                val itemV4 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_description),
                    midContent = getDescriptionByLanguageType(da)
                )

                val itemV5 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_package_id),
                    midContent = da.packageId
                )

                val itemV6 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_error_message),
                    midContent = errorMsg
                )

                val itemB = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_BOTTOM,
                    bottomType = VoucherInfoItemEntity.BOTTOM_WHITE_BUTTON_AND_GUIDE,
                    bottomGuideText = con.getString(R.string.string_ptk_more_record),
                    bottomButtonText = con.getString(R.string.string_ptk_all_finish),
                    pageType = VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_FAIL
                )

                list.add(itemTop)
                list.add(itemV1)
                list.add(itemV2)
                list.add(itemV3)
                list.add(itemV4)
                list.add(itemV5)
                list.add(itemV6)
                list.add(itemB)
            }
        }
        return list
    }


    /**
     * 核销成功记录
     */
    fun getWriteOffSuccessRecordData(
        context: Context?,
        data: VoucherInfoEntity?
    ): ArrayList<VoucherInfoItemEntity> {
        val list = ArrayList<VoucherInfoItemEntity>()
        list.clear()
        context?.let { con ->
            data?.let { da ->

                val itemH = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_HORIZONTAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_status),
                    midContent = con.getString(R.string.string_ptk_redeem_succeed),
                    midContentHorizontalColor = VoucherInfoItemEntity.COLOR_MID_CONTENT_SUCCESS
                )

                val itemV1 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_offer_name),
                    midContent = getVoucherNameByLanguageType(da)
                )

                val itemV2 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_id),
                    midContent = da.voucherId
                )

                val itemV3 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_value),
                    midContent = "${MainApplication.feeFh} ${da.offerNetAmount}"
                )

                val itemV4 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_description),
                    midContent = getDescriptionByLanguageType(da)
                )

                val itemV5 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_package_id),
                    midContent = da.packageId
                )

                val itemV6 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_redeem_time),
                    midContent = da.redeemTimeStr
                )

                list.add(itemH)
                list.add(itemV1)
                list.add(itemV2)
                list.add(itemV3)
                list.add(itemV4)
                list.add(itemV5)
                list.add(itemV6)
            }
        }
        return list
    }


    /**
     * 核销失败记录
     */
    fun getWriteOffFailedRecordData(
        context: Context?,
        data: VoucherInfoEntity?
    ): ArrayList<VoucherInfoItemEntity> {
        val list = ArrayList<VoucherInfoItemEntity>()
        list.clear()
        context?.let { con ->
            data?.let { da ->

                val itemH = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_HORIZONTAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_status),
                    midContent = con.getString(R.string.string_ptk_redeem_failed),
                    midContentHorizontalColor = VoucherInfoItemEntity.COLOR_MID_CONTENT_FAIL
                )

                val itemV1 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_offer_name),
                    midContent = getVoucherNameByLanguageType(da)
                )

                val itemV2 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_id),
                    midContent = da.voucherId
                )

                val itemV3 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_value),
                    midContent = "${MainApplication.feeFh} ${da.offerNetAmount}"
                )

                val itemV4 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_description),
                    midContent = getDescriptionByLanguageType(da)
                )

                val itemV5 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_package_id),
                    midContent = da.packageId
                )

                val itemV6 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_error_message),
                    midContent = da.thiMessage
                )

                list.add(itemH)
                list.add(itemV1)
                list.add(itemV2)
                list.add(itemV3)
                list.add(itemV4)
                list.add(itemV5)
                list.add(itemV6)
            }
        }
        return list
    }


    /**
     * 核销处理中记录
     */
    fun getWriteOffProcessingRecordData(
        context: Context?,
        data: VoucherInfoEntity?
    ): ArrayList<VoucherInfoItemEntity> {
        val list = ArrayList<VoucherInfoItemEntity>()
        list.clear()
        context?.let { con ->
            data?.let { da ->

                val itemH = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_HORIZONTAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_status),
                    midContent = con.getString(R.string.string_ptk_redeem_processing),
                    midContentHorizontalColor = VoucherInfoItemEntity.COLOR_MID_CONTENT_PROCESSING
                )

                val itemV1 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_offer_name),
                    midContent = getVoucherNameByLanguageType(da)
                )

                val itemV2 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_id),
                    midContent = da.voucherId
                )

                val itemV3 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_value),
                    midContent = "${MainApplication.feeFh} ${da.offerNetAmount}"
                )

                val itemV4 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_voucher_description),
                    midContent = getDescriptionByLanguageType(da)
                )

                val itemV5 = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
                    midTitle = con.getString(R.string.string_ptk_package_id),
                    midContent = da.packageId
                )


                val itemB = VoucherInfoItemEntity(
                    type = VoucherInfoItemEntity.ITEM_TYPE_BOTTOM,
                    bottomType = VoucherInfoItemEntity.BOTTOM_BLUE_BUTTON,
                    bottomButtonText = con.getString(R.string.string_ptk_retry),
                    pageType = VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_RECORD_PROCESSING
                )

                list.add(itemH)
                list.add(itemV1)
                list.add(itemV2)
                list.add(itemV3)
                list.add(itemV4)
                list.add(itemV5)
                list.add(itemB)
            }
        }
        return list
    }

}