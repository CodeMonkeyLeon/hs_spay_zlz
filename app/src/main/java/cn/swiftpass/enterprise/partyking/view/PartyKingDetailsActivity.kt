package cn.swiftpass.enterprise.partyking.view

import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.partyking.adapter.PartyKingDetailsAdapter
import cn.swiftpass.enterprise.partyking.entity.RedeemEntity
import cn.swiftpass.enterprise.partyking.entity.TimeOutErrorEntity
import cn.swiftpass.enterprise.partyking.entity.VoucherInfoEntity
import cn.swiftpass.enterprise.partyking.entity.VoucherInfoItemEntity
import cn.swiftpass.enterprise.partyking.event.PtkEvent
import cn.swiftpass.enterprise.partyking.manager.PtkManager
import cn.swiftpass.enterprise.partyking.utils.PtkUtils
import cn.swiftpass.enterprise.ui.activity.TemplateActivity
import cn.swiftpass.enterprise.ui.widget.TitleBar
import cn.swiftpass.enterprise.utils.ActivitySkipUtil
import cn.swiftpass.enterprise.utils.Logger
import cn.swiftpass.enterprise.utils.OnProhibitFastClickListener
import org.greenrobot.eventbus.EventBus

class PartyKingDetailsActivity : TemplateActivity() {


    private fun getLayoutId() = R.layout.act_party_king_details


    private lateinit var mRecycleView: RecyclerView
    private lateinit var mAdapter: PartyKingDetailsAdapter
    private var mList = mutableListOf<VoucherInfoItemEntity>()
    private var mToolBarTitle: String = ""
    private var mPageType: Int = 0

    private var mVoucherInfo: VoucherInfoEntity? = null

    private var mOutTradeNo: String? = null

    private lateinit var mLlBottomLayout: LinearLayout

    private var mPollCount: Int = 0

    companion object {
        const val TAG = "PartyKingDetailsActivity"
        const val PTK_DETAILS_DATA = "PTK_DETAILS_DATA"
        const val PTK_DETAILS_TOOLBAR_TITLE = "PTK_DETAILS_TOOLBAR_TITLE"
        const val PTK_PAGE_TYPE = "PTK_PAGE_TYPE"
        private const val PTK_VOUCHER_INFO = "PTK_VOUCHER_INFO"
        private const val PTK_OUT_TRADE_NO = "PTK_OUT_TRADE_NO"

        //最多轮询三次
        private const val MAX_POLL_COUNT = 3


        /**
         * @param fromActivity 启动Activity
         * @param data RecyclerView页面布局数据
         * @param pageType 页面的类型(因为是共用界面, 用于区分界面)
         * @param voucherInfo 核销结果信息
         * @param outTradeNo 更新"处理中"状态接口请求参数
         *
         */
        fun startPtkDetailsActivity(
            fromActivity: Activity,
            data: ArrayList<VoucherInfoItemEntity>,
            pageType: Int,
            toolBarTitle: String,
            voucherInfo: VoucherInfoEntity? = null,
            outTradeNo: String? = null
        ) {
            val hashMap = HashMap<String, Any>()
            hashMap[PTK_DETAILS_DATA] = data
            hashMap[PTK_DETAILS_TOOLBAR_TITLE] = toolBarTitle
            hashMap[PTK_PAGE_TYPE] = pageType
            voucherInfo?.let {
                hashMap[PTK_VOUCHER_INFO] = it
            }
            outTradeNo?.let {
                hashMap[PTK_OUT_TRADE_NO] = it
            }
            ActivitySkipUtil.startAnotherActivity(
                fromActivity,
                PartyKingDetailsActivity::class.java,
                hashMap
            )
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        mList.clear()
        intent?.let {
            val list = it.getSerializableExtra(PTK_DETAILS_DATA) as ArrayList<VoucherInfoItemEntity>
            mOutTradeNo = it.getStringExtra(PTK_OUT_TRADE_NO)
            it.getSerializableExtra(PTK_VOUCHER_INFO)?.let { info ->
                mVoucherInfo = info as VoucherInfoEntity
            }
            mToolBarTitle = it.getStringExtra(PTK_DETAILS_TOOLBAR_TITLE).toString()
            mList.addAll(list)
            setPageType(it.getIntExtra(PTK_PAGE_TYPE, 0))
        }
        initToolBar()
        initView()
        initBottomLayout()
    }


    private fun initBottomLayout() {
        mLlBottomLayout = findViewById(R.id.id_ll_ptk_bottom_layout)
        val bottomItem = mList[mList.size - 1]
        if (mList.size > 0 && bottomItem.type == VoucherInfoItemEntity.ITEM_TYPE_BOTTOM) {
            mLlBottomLayout.visibility = View.VISIBLE
            setBottomLayout(mLlBottomLayout, bottomItem)
        }
    }


    /**
     * 设置底部布局
     */
    private fun setBottomLayout(layout: View, item: VoucherInfoItemEntity?) {
        item?.let { data ->
            val guideTextView = layout.findViewById<TextView>(R.id.id_tv_title)
            val blueButton = layout.findViewById<Button>(R.id.id_btn_type_blue)
            val whiteButton = layout.findViewById<Button>(R.id.id_btn_type_white)

            //设置布局
            guideTextView.text = data.bottomGuideText
            when (data.bottomType) {
                VoucherInfoItemEntity.BOTTOM_NO_BUTTON -> {
                    //底部没有按钮
                    blueButton.visibility = View.GONE
                    whiteButton.visibility = View.GONE
                }
                VoucherInfoItemEntity.BOTTOM_BLUE_BUTTON_AND_GUIDE -> {
                    //蓝色按钮 + tips
                    whiteButton.visibility = View.GONE
                    blueButton.visibility = View.VISIBLE
                    blueButton.text = data.bottomButtonText
                }
                VoucherInfoItemEntity.BOTTOM_WHITE_BUTTON_AND_GUIDE -> {
                    //白色按钮 + tips
                    blueButton.visibility = View.GONE
                    whiteButton.visibility = View.VISIBLE
                    whiteButton.text = data.bottomButtonText
                }
                VoucherInfoItemEntity.BOTTOM_BLUE_BUTTON -> {
                    //只有蓝色按钮
                    guideTextView.visibility = View.GONE
                    whiteButton.visibility = View.GONE
                    blueButton.visibility = View.VISIBLE
                    blueButton.text = data.bottomButtonText
                    if (item.pageType == VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_CAN_NOT_WRITE_OFF) {
                        blueButton.isEnabled = false
                    }
                }
                VoucherInfoItemEntity.BOTTOM_WHITE_BUTTON -> {
                    //只有白色按钮
                    guideTextView.visibility = View.GONE
                    blueButton.visibility = View.GONE
                    whiteButton.visibility = View.VISIBLE
                    whiteButton.text = data.bottomButtonText
                }

            }


            //点击引导文字
            guideTextView.setOnClickListener(object : OnProhibitFastClickListener() {
                override fun onFilterClick(v: View?) {
                    dealWithGuildTextClickEvent(data)
                }
            })

            //点击 蓝色按钮
            blueButton.setOnClickListener(object : OnProhibitFastClickListener() {
                override fun onFilterClick(v: View?) {
                    dealWithBlueButtonClickEvent(data)
                }
            })

            //点击 白色按钮
            whiteButton.setOnClickListener(object : OnProhibitFastClickListener() {
                override fun onFilterClick(v: View?) {
                    dealWithWhiteButtonClickEvent(data)
                }
            })
        }
    }


    private fun initView() {
        //初始化RecyclerView
        mRecycleView = findViewById(R.id.id_recycler_view_party_king)
        val layoutManager = LinearLayoutManager(activity)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        mRecycleView.layoutManager = layoutManager

        mAdapter = PartyKingDetailsAdapter(activity, mList as ArrayList<VoucherInfoItemEntity>)
//        mAdapter.setOnPktButtonLayoutListener(object : OnPktBottomLayoutListener {
////            override fun onPktBottomLayoutSize(bottomLayout: LinearLayout) {
////                Logger.d(TAG, "---- ptk 底部已绘制完成")
////            }
//
//            override fun onPktBottomGuideTextClick(data: VoucherInfoItemEntity?) {
//                Logger.d(TAG, "---- ptk 点击底部引导文字")
//                data?.let {
//                    dealWithGuildTextClickEvent(it)
//                }
//            }
//
//            override fun onPktBottomBlueButtonClick(data: VoucherInfoItemEntity?) {
//                Logger.d(TAG, "---- ptk 点击底部蓝色按钮")
//                data?.let {
//                    dealWithBlueButtonClickEvent(it)
//                }
//            }
//
//            override fun onPktBottomWhiteButtonClick(data: VoucherInfoItemEntity?) {
//                Logger.d(TAG, "---- ptk 点击底部白色按钮")
//                data?.let {
//                    dealWithWhiteButtonClickEvent(it)
//                }
//            }
//        })

        mRecycleView.adapter = mAdapter
    }


    private fun dealWithGuildTextClickEvent(data: VoucherInfoItemEntity) {
        when (data.pageType) {
            VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_SUCCESS,
            VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_FAIL,
            VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_PROCESSING -> {
                //处于核销成功/失败/处理中界面
                PartyKingSummaryActivity.startPtkSummaryActivity(
                    activity
                )
                finish()
            }
        }
    }


    //    private var blueNumber = 0
    private fun dealWithBlueButtonClickEvent(data: VoucherInfoItemEntity) {
        when (data.pageType) {
            VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_CONFIRM -> {
                //处于核销确认界面
                mVoucherInfo?.let { info ->
                    PtkManager.doRedeem(
                        info.voucherId,
                        info.signature,
                        object : UINotifyListener<RedeemEntity>() {
                            override fun onPreExecute() {
                                super.onPreExecute()
                                loadDialog(activity, getString(R.string.loading))
                            }


                            override fun onError(error: Any?) {
                                super.onError(error)
                                dismissLoading()
                                if (checkSession()) {
                                    return
                                }
                                runOnUiThread {
                                    error?.let { e ->
                                        toastDialog(activity, e.toString(), null)
                                    }
                                }
                            }

                            override fun onSucceed(result: RedeemEntity?) {
                                super.onSucceed(result)
                                dismissLoading()
                                result?.let { res ->
                                    goToNextPageByPtkVoucherState(res)
                                }
                            }
                        })
                }
            }
            VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_CAN_NOT_WRITE_OFF -> {
                //处于不可核销界面
                //按钮置灰, 不可点击
            }
            VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_PROCESSING -> {
                //处于核销处理中界面
                updateVoucherState(false)
            }
            VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_RECORD_PROCESSING -> {
                //处于核销记录处理中界面
//                EventBus.getDefault().postSticky(
//                    PtkEvent(
//                        PtkEvent.EVENT_UPDATE_PTK_SUMMARY_LIST,
//                        "update ptk summary list"
//                    )
//                )
                updateVoucherState(true)
            }
        }
    }


    /**
     * 处理中 状态，点击更新状态
     */
    private fun updateVoucherState(isRecordPage: Boolean) {
        mVoucherInfo?.let { info ->
            mOutTradeNo?.let { no ->
                PtkManager.retryCheckRedeem(
                    no,
                    info.voucherId,
                    info.signature,
                    object : UINotifyListener<RedeemEntity>() {

                        override fun onPreExecute() {
                            super.onPreExecute()
                            loadDialog(activity, getString(R.string.loading))
                        }

                        override fun onError(error: Any?) {
                            super.onError(error)
                            dismissLoading()
                            error?.let {
                                if (isRecordPage) {
                                    //记录界面才进行轮询
                                    if (error is TimeOutErrorEntity) {
                                        ///连接逾时,再轮询三次
                                        mPollCount++
                                        if (mPollCount > 3) {
                                            mPollCount = 0
                                            if (checkSession()) {
                                                return
                                            }
                                            runOnUiThread {
                                                toastDialog(
                                                    activity,
                                                    (error as TimeOutErrorEntity).errorMsg,
                                                    null
                                                )
                                            }
                                        } else {
                                            runOnUiThread {
                                                updateVoucherState(isRecordPage)
                                            }
                                        }
                                    } else {
                                        //非链接逾时错误
                                        mPollCount = 0
                                        if (checkSession()) {
                                            return
                                        }
                                        runOnUiThread {
                                            toastDialog(
                                                activity,
                                                error.toString(),
                                                null
                                            )
                                        }
                                    }
                                } else {
                                    mPollCount = 0
                                    if (checkSession()) {
                                        return
                                    }
                                    if (error is TimeOutErrorEntity) {
                                        runOnUiThread {
                                            toastDialog(
                                                activity,
                                                error.errorMsg,
                                                null
                                            )
                                        }
                                    } else {
                                        runOnUiThread {
                                            toastDialog(
                                                activity,
                                                error.toString(),
                                                null
                                            )
                                        }
                                    }
                                }
                            }
                        }

                        override fun onSucceed(result: RedeemEntity?) {
                            super.onSucceed(result)
                            dismissLoading()
                            mPollCount = 0
                            result?.let { res ->
                                if (isRecordPage) {
                                    EventBus.getDefault().postSticky(
                                        PtkEvent(
                                            PtkEvent.EVENT_UPDATE_PTK_SUMMARY_LIST,
                                            "update ptk summary list"
                                        )
                                    )
                                }
                                goToNextPageByPtkVoucherState(res, isRecordPage)
                            }
                        }

                    })
            }

        }
    }


    private fun dealWithWhiteButtonClickEvent(data: VoucherInfoItemEntity) {
        when (data.pageType) {
            VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_SUCCESS,
            VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_FAIL -> {
                //处于核销成功/失败界面
                EventBus.getDefault()
                    .postSticky(PtkEvent(PtkEvent.EVENT_STOP_SCAN_CAPTURE, "'stop scan"))
                finish()
            }
        }
    }

    private fun initToolBar() {
        titleBar.setLeftButtonVisible(true)
        titleBar.setRightButLayVisibleForTotal(false, "")
        titleBar.setTitle(mToolBarTitle)
        titleBar.setOnTitleBarClickListener(object : TitleBar.OnTitleBarClickListener {
            override fun onLeftButtonClick() {
                //返回
                finish()
            }

            override fun onRightButtonClick() {

            }

            override fun onRightLayClick() {

            }

            override fun onRightButLayClick() {
            }

        })
    }


    private fun setPageType(type: Int) {
        mPageType = type
        Logger.d(TAG, "--- mPageType : $mPageType")
    }


    /**
     * 根据 核销状态 判断下一个界面类型
     */
    private fun goToNextPageByPtkVoucherState(res: RedeemEntity, isRecordPage: Boolean = false) {
        mVoucherInfo?.let { voucherInfo ->
            voucherInfo.tradeState = res.tradeState
            voucherInfo.voucherState = res.voucherState
            voucherInfo.redeemTimeStr = res.redeemTimeStr
            voucherInfo.thiMessage = res.thiMessage
        }

        when (res.tradeState.toInt()) {
            RedeemEntity.REDEEM_SUCCESS -> {
                //核销成功
                if (isRecordPage) {
                    //核销成功记录界面
                    startPtkDetailsActivity(
                        activity,
                        PtkUtils.getWriteOffSuccessRecordData(activity, mVoucherInfo),
                        VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_RECORD_SUCCESS,
                        getString(R.string.string_ptk_voucher)
                    )
                } else {
                    //核销成功界面
                    startPtkDetailsActivity(
                        activity,
                        PtkUtils.getWriteOffSuccessData(
                            activity,
                            mVoucherInfo,
                            res.redeemTimeStr
                        ),
                        VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_SUCCESS,
                        getString(R.string.string_ptk_voucher),
                        mVoucherInfo
                    )
                }
            }
            RedeemEntity.REDEEM_FAILED -> {
                //核销失败
                if (isRecordPage) {
                    //核销失败记录界面
                    startPtkDetailsActivity(
                        activity,
                        PtkUtils.getWriteOffFailedRecordData(activity, mVoucherInfo),
                        VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_RECORD_FAIL,
                        getString(R.string.string_ptk_voucher)
                    )
                } else {
                    //核销失败界面
                    startPtkDetailsActivity(
                        activity,
                        PtkUtils.getWriteOffFailedData(
                            activity,
                            mVoucherInfo,
                            res.thiMessage
                        ),
                        VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_FAIL,
                        getString(R.string.string_ptk_voucher),
                        mVoucherInfo
                    )
                }
            }
            RedeemEntity.REDEEM_PROCESSING -> {
                //处理中
                val outTradeNo =
                    if (TextUtils.isEmpty(res.outTradeNo)) mOutTradeNo else res.outTradeNo
                if (isRecordPage) {
                    //处理中记录界面
                    startPtkDetailsActivity(
                        activity,
                        PtkUtils.getWriteOffProcessingRecordData(activity, mVoucherInfo),
                        VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_RECORD_PROCESSING,
                        getString(R.string.string_ptk_voucher),
                        mVoucherInfo,
                        outTradeNo
                    )
                } else {
                    //处理中界面
                    startPtkDetailsActivity(
                        activity,
                        PtkUtils.getWriteOffProcessingData(
                            activity,
                            mVoucherInfo
                        ),
                        VoucherInfoItemEntity.PAGE_TYPE_WRITE_OFF_PROCESSING,
                        getString(R.string.string_ptk_voucher),
                        mVoucherInfo,
                        outTradeNo
                    )
                }
            }
        }
        runOnUiThread {
            finish()
        }
    }


//    private fun test(data: VoucherInfoItemEntity) {
//        when (data.bottomPageType) {
//            VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_CONFIRM -> {
//                //处于核销确认界面
//            }
//            VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_CAN_NOT_WRITE_OFF -> {
//                //处于不可核销界面
//            }
//            VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_WRITE_OFF_SUCCESS -> {
//                //处于核销成功界面
//            }
//            VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_WRITE_OFF_FAIL -> {
//                //处于核销失败界面
//            }
//            VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_WRITE_OFF_TIMEOUT -> {
//                //处于核销超时界面
//            }
//            VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_ROLLBACK_SUCCESS -> {
//                //处于撤回成功界面
//            }
//            VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_RECORD_SUCCESS -> {
//                //处于核销记录成功界面
//            }
//            VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_RECORD_FAIL -> {
//                //处于核销记录失败界面
//            }
//            VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_RECORD_TIMEOUT -> {
//                //处于核销记录超时界面
//            }
//            VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_RECORD_ROLLBACK -> {
//                //处于核销记录撤回界面
//            }
//        }
//    }


}