package cn.swiftpass.enterprise.partyking.adapter

import android.content.Context
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.partyking.entity.PtkItemEntity
import cn.swiftpass.enterprise.partyking.interfaces.OnPtkSummaryItemClickListener
import cn.swiftpass.enterprise.partyking.utils.PtkUtils
import cn.swiftpass.enterprise.utils.Logger
import cn.swiftpass.enterprise.utils.OnProhibitFastClickListener
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder

class PartyKingSummaryAdapter(val mContext: Context, val mList: List<PtkItemEntity>) :
    BaseMultiItemQuickAdapter<PtkItemEntity, BaseViewHolder>(mList) {


    companion object {
        const val TAG = "PartyKingSummaryAdapter"
    }


    private var mOnPtkSummaryItemClickListener: OnPtkSummaryItemClickListener? = null

    fun setOnPtkSummaryItemClickListener(listener: OnPtkSummaryItemClickListener) {
        mOnPtkSummaryItemClickListener = listener
    }


    init {
        addItemType(PtkItemEntity.TYPE_PTK_SUMMARY_DEFAULT, R.layout.item_pk_summary)
    }


    override fun convert(helper: BaseViewHolder?, item: PtkItemEntity?) {
        helper?.let { h ->
            Logger.d(
                TAG,
                "--- itemViewType : ${h.itemViewType}    position : ${h.position}    adapterPosition: ${h.adapterPosition}"
            )
            if (h.itemViewType == PtkItemEntity.TYPE_PTK_SUMMARY_DEFAULT) {
                item?.let {
                    setLayout(h, it, h.adapterPosition)
                }
            }
        }
    }


    private fun setLayout(helper: BaseViewHolder, item: PtkItemEntity, position: Int) {
        //设置内容
        helper.setText(
            R.id.id_tv_pk_summary_item_title,
            PtkUtils.getVoucherNameByLanguageTypePtkSummary(item)
        )
        helper.setText(R.id.id_tv_pk_summary_item_time, item.voucherTime)
        helper.setText(R.id.id_tv_money, "${MainApplication.feeFh} ${item.offerNetAmount}")

        val tvMoney = helper.itemView.findViewById<TextView>(R.id.id_tv_money)
        tvMoney.post {
            if (tvMoney.lineCount > 1) {
                //超过一行，换行处理
                tvMoney.text = "${MainApplication.feeFh}\n${item.offerNetAmount}"
            }
        }


        val statusTextView =
            helper.itemView.findViewById<TextView>(R.id.id_tv_pk_summary_item_status)
        when (item.tradeState) {
            //处理中
            PtkItemEntity.STATUS_TYPE_PROCESSING -> {
                helper.setText(
                    R.id.id_tv_pk_summary_item_status,
                    mContext.getString(R.string.string_ptk_redeem_processing)
                )
                statusTextView.setTextColor(
                    mContext.resources.getColor(
                        R.color.pk_summary_status_processing
                    )
                )
            }
            //成功
            PtkItemEntity.STATUS_TYPE_SUCCEED -> {
                helper.setText(
                    R.id.id_tv_pk_summary_item_status,
                    mContext.getString(R.string.string_ptk_redeem_succeed)
                )
                statusTextView.setTextColor(
                    mContext.resources.getColor(
                        R.color.pk_summary_status_succeed
                    )
                )
            }
            //失败
            PtkItemEntity.STATUS_TYPE_FAILED -> {
                helper.setText(
                    R.id.id_tv_pk_summary_item_status,
                    mContext.getString(R.string.string_ptk_redeem_failed)
                )
                statusTextView.setTextColor(
                    mContext.resources.getColor(
                        R.color.pk_summary_status_failed
                    )
                )
            }
        }

        //item点击事件
        val itemRelativeLayout =
            helper.itemView.findViewById<RelativeLayout>(R.id.id_rl_pk_summary_item_layout)
        itemRelativeLayout.setOnClickListener(object : OnProhibitFastClickListener() {
            override fun onFilterClick(v: View?) {
                mOnPtkSummaryItemClickListener?.let {
                    it.onPtkSummaryItemClick(v, position, item)
                }
            }
        })


    }
}