package cn.swiftpass.enterprise.partyking.interfaces

import cn.swiftpass.enterprise.partyking.entity.VoucherInfoItemEntity

interface OnPktBottomLayoutListener {

//    //底部布局尺寸
//    fun onPktBottomLayoutSize(bottomLayout: LinearLayout)

    //点击 引导文字
    fun onPktBottomGuideTextClick(data: VoucherInfoItemEntity?)

    //点击 蓝色按钮
    fun onPktBottomBlueButtonClick(data: VoucherInfoItemEntity?)

    //点击 白色按钮
    fun onPktBottomWhiteButtonClick(data: VoucherInfoItemEntity?)
}