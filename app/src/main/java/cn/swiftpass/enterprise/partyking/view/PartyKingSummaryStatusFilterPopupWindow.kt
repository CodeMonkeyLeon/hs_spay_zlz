package cn.swiftpass.enterprise.partyking.view

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.PopupWindow
import android.widget.TextView
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.partyking.interfaces.OnPartyKingSummaryStatusFilterOkClickListener

class PartyKingSummaryStatusFilterPopupWindow(
    val mContext: Context,
    private var mChooseType: String = "",
    private var mOnOkClickListener: OnPartyKingSummaryStatusFilterOkClickListener?
) : PopupWindow(mContext) {


    companion object {
        //状态 1-处理中 2-已核销 3-核销失败
        const val TYPE_REDEEM_STATE_PROCESSING = "1"
        const val TYPE_REDEEM_STATE_SUCCESS = "2"
        const val TYPE_REDEEM_STATE_FAIL = "3"
        const val TYPE_REDEEM_STATE_ALL = ""
    }


    private lateinit var mMenuView: View
    private lateinit var btnReset: TextView
    private lateinit var btnSuccess: Button
    private lateinit var btnFail: Button
    private lateinit var btnProcessing: Button
    private lateinit var btnOk: TextView

    init {
        val inflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        mMenuView = inflater.inflate(R.layout.layout_ptk_summary_status_popup_window, null)
        //设置PopupWindow的View
        this.contentView = mMenuView
        //设置PopupWindow弹出窗体的宽
        this.width = ViewGroup.LayoutParams.MATCH_PARENT
        //设置PopupWindow弹出窗体的高
        this.height = ViewGroup.LayoutParams.WRAP_CONTENT
        //设置PopupWindow弹出窗体可点击
        this.isFocusable = true
        //设置PopupWindow弹出窗体动画效果
        this.animationStyle = R.style.AnimBottom
        //设置PopupWindow弹出窗体的背景半透明
        this.setBackgroundDrawable(ColorDrawable(-0x50000000))
        initView()
    }


    private fun initView() {
        btnReset = mMenuView.findViewById(R.id.tv_reset)
        btnSuccess = mMenuView.findViewById(R.id.tv_succeed)
        btnFail = mMenuView.findViewById(R.id.tv_fail)
        btnProcessing = mMenuView.findViewById(R.id.tv_processing)
        btnOk = mMenuView.findViewById(R.id.btn__ok)


        when (mChooseType) {
            TYPE_REDEEM_STATE_PROCESSING -> choose(btnProcessing)
            TYPE_REDEEM_STATE_SUCCESS -> choose(btnSuccess)
            TYPE_REDEEM_STATE_FAIL -> choose(btnFail)
            TYPE_REDEEM_STATE_ALL -> reset()
        }

        //重置
        btnReset.setOnClickListener {
            reset()
        }

        //核销成功
        btnSuccess.setOnClickListener {
            updateButtonBackground(it as TextView, TYPE_REDEEM_STATE_SUCCESS)
        }

        //核销失败
        btnFail.setOnClickListener {
            updateButtonBackground(it as TextView, TYPE_REDEEM_STATE_FAIL)
        }

        //核销超时
        btnProcessing.setOnClickListener {
            updateButtonBackground(it as TextView, TYPE_REDEEM_STATE_PROCESSING)
        }

        //确认
        btnOk.setOnClickListener {
            mOnOkClickListener?.let {
                it.onPartyKingSummaryStatusFilterOkClick(mChooseType)
            }
            dismiss()
        }
    }


    /**
     * 重置按钮状态
     */
    private fun reset() {
        mChooseType = TYPE_REDEEM_STATE_ALL
        unChoose(btnSuccess as TextView)
        unChoose(btnFail as TextView)
        unChoose(btnProcessing as TextView)
    }


    /**
     * 更新按钮状态
     */
    private fun updateButtonBackground(button: TextView, type: String) {
        reset()
        mChooseType = type
        choose(button)
    }


    /**
     * 选中
     */
    private fun choose(button: TextView) {
        button.setBackgroundResource(R.drawable.button_shape)
        button.setTextColor(Color.WHITE)
    }


    /**
     * 取消选中
     */
    private fun unChoose(button: TextView) {
        button.setBackgroundResource(R.drawable.general_button_thickness_white_default)
        button.setTextColor(Color.parseColor("#535353"))
    }


}