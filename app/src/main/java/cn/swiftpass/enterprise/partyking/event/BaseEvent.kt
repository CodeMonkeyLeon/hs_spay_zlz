package cn.swiftpass.enterprise.partyking.event

import java.io.Serializable

open class BaseEvent(
    private var eventType: Int = 0,
    private var eventMessage: String = ""
) : Serializable