package cn.swiftpass.enterprise.partyking.interfaces

import android.view.View

interface OnDialogOkClickListener {
    fun onDialogOkClick(view: View)
}