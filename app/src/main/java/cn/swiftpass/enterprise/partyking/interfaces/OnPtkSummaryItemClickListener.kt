package cn.swiftpass.enterprise.partyking.interfaces

import android.view.View
import cn.swiftpass.enterprise.partyking.entity.PtkItemEntity

interface OnPtkSummaryItemClickListener {
    fun onPtkSummaryItemClick(view: View?, position: Int = 0, item: PtkItemEntity)
}