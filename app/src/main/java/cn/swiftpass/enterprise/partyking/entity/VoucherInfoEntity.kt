package cn.swiftpass.enterprise.partyking.entity

import com.chad.library.adapter.base.entity.MultiItemEntity
import java.io.Serializable

data class VoucherInfoEntity(
    var campaignName: String = "",
    var offerCurrencyType: String = "",
    var offerNetAmount: String = "",
    var packageId: String = "",
    var platformVoucherId: String = "",
    var redeemTimeStr: String = "",
    var signature: String = "",
    var thiCode: String = "",
    var thiMessage: String = "",
    var tradeState: String = "",
    var voucherDescChs: String = "",
    var voucherDescCht: String = "",
    var voucherDescEn: String = "",
    var voucherDisplayNameChs: String = "",
    var voucherDisplayNameCht: String = "",
    var voucherDisplayNameEn: String = "",
    var voucherId: String = "",
    var voucherState: String = ""
) : Serializable {
    companion object {
        const val VOUCHER_STATE_ACTIVE = "ACTIVE"
        const val VOUCHER_STATE_INACTIVE = "INACTIVE"
        const val VOUCHER_STATE_REDEEMED = "REDEEMED"
    }
}

data class PtkVoucherInfoParams(
    var packageId: String,
    var signature: String
) : Serializable


data class RedeemEntity(
    var tradeState: String = "",
    var voucherState: String = "",
    var redeemTimeStr: String = "",
    var outTradeNo: String = "",
    var thiMessage: String = "",
) : Serializable {
    companion object {
        //状态 1-处理中 2-已核销 3-核销失败
        const val REDEEM_PROCESSING = 1
        const val REDEEM_SUCCESS = 2
        const val REDEEM_FAILED = 3
    }
}


data class PtkSummaryEntity(
    var otherNum: String = "",
    var successNum: String = "",
    var totalNum: String = ""
) : Serializable


data class PtkListEntity(
    var currentPage: String = "",
    var `data`: ArrayList<PtkItemEntity> = ArrayList<PtkItemEntity>(),
    var pageCount: String = "",
    var perPage: String = "",
    var reqFeqTime: String = "",
    var totalRows: String = ""
) : Serializable

data class PtkItemEntity(
    var campaignName: String = "",
    var offerCurrencyType: String = "",
    var offerNetAmount: String = "",
    var tradeState: String = "",
    var voucherDisplayNameChs: String = "",
    var voucherDisplayNameCht: String = "",
    var voucherDisplayNameEn: String = "",
    var voucherOrderId: String = "",
    var voucherTime: String = ""
) : Serializable, MultiItemEntity {


    companion object {
        const val TYPE_PTK_SUMMARY_DEFAULT = 0

        //状态 1-处理中 2-已核销 3-核销失败
        const val STATUS_TYPE_PROCESSING = "1"
        const val STATUS_TYPE_SUCCEED = "2"
        const val STATUS_TYPE_FAILED = "3"
    }


    override fun getItemType() = TYPE_PTK_SUMMARY_DEFAULT
}


data class PtkDetailEntity(
    var campaignName: String = "",
    var offerCurrencyType: String = "",
    var offerNetAmount: String = "",
    var outTradeNo: String = "",
    var packageId: String = "",
    var redeemTimeStr: String = "",
    var thiMessage: String = "",
    var tradeState: String = "",
    var voucherDescChs: String = "",
    var voucherDescCht: String = "",
    var voucherDescEn: String = "",
    var voucherDisplayNameChs: String = "",
    var voucherDisplayNameCht: String = "",
    var voucherDisplayNameEn: String = "",
    var voucherId: String = "",
    var voucherOrderId: String = "",
    var signature: String = ""
) : Serializable

data class TimeOutErrorEntity(
    var errorCode: Int,
    var errorMsg: String
) : Serializable