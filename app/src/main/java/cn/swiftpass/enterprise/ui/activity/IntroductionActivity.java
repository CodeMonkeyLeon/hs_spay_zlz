/*
 * 文 件 名:  ApplyActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-8-6
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity;

import android.os.Bundle;
import cn.swiftpass.enterprise.intl.R;

/**
 * 快速认识产品
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2014-8-6]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class IntroductionActivity extends TemplateActivity
{
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.introduction);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.introduction_title);
    }
    
}
