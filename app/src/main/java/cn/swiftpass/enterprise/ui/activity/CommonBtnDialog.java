package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.widget.dialog.ChangePreauthDialog;

/**
 * Created by aijingya on 2018/12/4.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/12/4.20:25.
 */

public class CommonBtnDialog extends Dialog {
    private static final String TAG = CommonBtnDialog.class.getSimpleName();
    private Context mContext;
    private TextView tv_message;
    private TextView btn_try_again,btn_cancel;

    private View.OnClickListener TryAgainButtonListener;

    public CommonBtnDialog(Activity context , View.OnClickListener ButtonListener) {
        super(context, R.style.simpleDialog);
        TryAgainButtonListener = ButtonListener;
        mContext = context;

        init(context);
        setCancelable(true);
    }

    private void init(Context context){
        View rootView = View.inflate(context, R.layout.dialog_common_btn_two, null);
        setContentView(rootView);

        tv_message = (TextView)rootView.findViewById(R.id.tv_error_message);
        btn_try_again = rootView.findViewById(R.id.btn_try_again);
        btn_cancel = rootView.findViewById(R.id.btn_cancel);

        btn_try_again.setOnClickListener(TryAgainButtonListener);

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

    public void  setMessage(String message){
        if(!TextUtils.isEmpty(message)){
            tv_message.setText(message);
        }else{
            Toast.makeText(mContext,"with no message",Toast.LENGTH_SHORT).show();
        }
    }
}
