package cn.swiftpass.enterprise.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.BindSpeakerBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.list.PullToRefreshBase;
import cn.swiftpass.enterprise.ui.activity.list.PullToRefreshListView;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * Created by aijingya on 2019/8/14.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: $(云播报添加和列表页面)
 * @date 2019/8/14.18:15.
 */
public class MySpeakerListActivity extends TemplateActivity {
    @BindView(R.id.ll_add_speaker)
    LinearLayout ll_add_speaker;
    @BindView(R.id.ll_no_speaker_add)
    LinearLayout ll_no_speaker_add;
    @BindView(R.id.pullrefresh_speaker_list)
    PullToRefreshListView pullrefresh_speaker_list;
    @BindView(R.id.rl_speaker_list)
    RelativeLayout rl_speaker_list;

    private ViewHolder holder;
    private ListView listView;
    private SpeakerAdapter mBindSpeakerAdapter;
    public ArrayList<BindSpeakerBean> bindSpeakerBeanArrayList = new ArrayList<BindSpeakerBean>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_speaker_list);
        ButterKnife.bind(this);
        initView();

    }

    @Override
    protected void onResume() {
        super.onResume();

        bindSpeakerBeanArrayList.clear();
        //请求网络获取当前账号是否绑定云音箱
        requestSpeakerList();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        bindSpeakerBeanArrayList.clear();

        //请求网络获取当前账号是否绑定云音箱
        requestSpeakerList();
    }


    public void initView() {
        listView = pullrefresh_speaker_list.getRefreshableView();
        mBindSpeakerAdapter = new SpeakerAdapter(bindSpeakerBeanArrayList);
        listView.setAdapter(mBindSpeakerAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //带数据跳转到绑定管理详情页
                Intent intent = new Intent();
                intent.putExtra("SpeakerBean", bindSpeakerBeanArrayList.get(position - 1));
                intent.setClass(MySpeakerListActivity.this, MySpeakerBindActivity.class);
                startActivity(intent);
            }
        });

        PullToRefreshBase.OnRefreshListener mOnrefreshListener = new PullToRefreshBase.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (pullrefresh_speaker_list.getRefreshType() == PullToRefreshBase.MODE_PULL_DOWN_TO_REFRESH) {
                    //下拉刷新数据
                    bindSpeakerBeanArrayList.clear();
                    //请求网络,刷新列表数据
                    requestSpeakerList();
                } else if (pullrefresh_speaker_list.getRefreshType() == PullToRefreshBase.MODE_PULL_UP_TO_REFRESH) {
                    //上拉刷新数据
                    pullrefresh_speaker_list.onRefreshComplete();
                }
            }
        };
        pullrefresh_speaker_list.setOnRefreshListener(mOnrefreshListener);
    }

    public void initData() {
        if(bindSpeakerBeanArrayList.size() > 0){
            ll_no_speaker_add.setVisibility(View.GONE);
            rl_speaker_list.setVerticalGravity(View.VISIBLE);

            mBindSpeakerAdapter.notifyDataSetChanged();
            pullrefresh_speaker_list.onRefreshComplete();
            listView.setSelection(0);

            //如果是收银员登录，切列表有数据，则不展示右上角的添加按钮
            if(MainApplication.isAdmin.equals("0")){
                titleBar.setRightButLayVisible(false, 0);
            }else if(MainApplication.isAdmin.equals("1")){ //如果是商户登录，切列表有数据，则判断商户是否绑定过音箱
                String mch;
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    mch = MainApplication.merchantId;
                } else {
                    mch = MainApplication.getMchId();
                }
                if(bindSpeakerBeanArrayList.get(0).getAccount().trim().equals(mch)){
                    //如果商户绑定过了，则不展示添加按钮
                    titleBar.setRightButLayVisible(false, 0);
                }else{
                    titleBar.setRightButLayVisible(true, 0);
                    titleBar.setRightButLayBackground(R.drawable.icon_add);
                }
            }

        }else{
            ll_no_speaker_add.setVisibility(View.VISIBLE);
            rl_speaker_list.setVerticalGravity(View.GONE);
           /* 不管是商户还是收银员，如果没有数据表示没有绑定过音箱，则不展示右上角的按钮
            只展示添加按钮*/
            titleBar.setRightButLayVisible(false, 0);
        }
    }


    //请求网络获取绑定的列表数据
    public void requestSpeakerList() {
        UserManager.getSpeakerList(new UINotifyListener<List<BindSpeakerBean>>() {
            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
                if (checkSession()) {
                    return;
                }
                if(object != null ){
                    toastDialog(MySpeakerListActivity.this, object.toString(), null);
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showLoading(false, R.string.public_loading);
            }

            @Override
            protected void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(List<BindSpeakerBean> result) {
                super.onSucceed(result);
                dismissLoading();
                bindSpeakerBeanArrayList.clear();
                bindSpeakerBeanArrayList.addAll(result);
                initData();
            }
        });
    }

    @OnClick(R.id.ll_add_speaker)
    public void onViewClicked() {
        showPage(MySpeakerIdInputActivity.class);
    }

    private class SpeakerAdapter extends BaseAdapter {
        private List<BindSpeakerBean> speakerBeanList;

        public SpeakerAdapter() {
        }

        public SpeakerAdapter(List<BindSpeakerBean> speakerBeanLists) {
            this.speakerBeanList = speakerBeanLists;
        }

        @Override
        public int getCount() {
            return speakerBeanList.size();
        }

        @Override
        public Object getItem(int position) {
            return speakerBeanList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(MySpeakerListActivity.this, R.layout.item_my_speaker_list, null);
                holder = new ViewHolder();
                holder.tv_user_name = convertView.findViewById(R.id.tv_user_name);
                holder.tv_speaker_id = convertView.findViewById(R.id.tv_speaker_id);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            BindSpeakerBean Bean = speakerBeanList.get(position);
            if (Bean != null) {
                if (!TextUtils.isEmpty(Bean.getUserName())) {
                    holder.tv_user_name.setText(Bean.getUserName());
                }
                if(!TextUtils.isEmpty(Bean.getDeviceName())){
                    holder.tv_speaker_id.setText(Bean.getDeviceName());
                }

            }
            return convertView;
        }
    }

    private class ViewHolder {
        private TextView tv_user_name, tv_speaker_id;
    }


    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(getStringById(R.string.terminal_my_speaker_title));
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButtonVisible(false);
        titleBar.setRightButLayVisible(true, 0);
        titleBar.setRightButLayBackground(R.drawable.icon_add);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onRightLayClick() {
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightButLayClick() {
                showPage(MySpeakerIdInputActivity.class);
            }

            @Override
            public void onLeftButtonClick() {
                finish();
            }
        });
    }
}
