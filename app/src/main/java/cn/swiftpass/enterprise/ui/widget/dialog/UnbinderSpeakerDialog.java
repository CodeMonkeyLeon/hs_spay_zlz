package cn.swiftpass.enterprise.ui.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;

/**
 * Created by aijingya on 2019/8/26.
 *
 * @Package cn.swiftpass.enterprise.ui.widget.dialog
 * @Description: (解绑云播报音箱的dialog)
 * @date 2019/8/26.13:56.
 */
public class UnbinderSpeakerDialog extends Dialog {
    Context mContext;
    TextView tv_merchant_name,tv_merchant_id,tv_speaker_id,btn_cancel,btn_confirm;

    private View.OnClickListener confirmButtonListener;
    String mch_name,mch_id,speaker_id;

    public UnbinderSpeakerDialog(Context context,String MerchantName,String MerchantId,String SpeakerId,View.OnClickListener ButtonListener) {
        super(context, R.style.simpleDialog);
        this.mContext = context;
        this.mch_name = MerchantName;
        this.mch_id = MerchantId;
        this.speaker_id = SpeakerId;
        confirmButtonListener = ButtonListener;
        initView(context);
        setCancelable(true);
    }

    public void initView(Context context) {
        View rootView = View.inflate(context, R.layout.dialog_unbind_speaker, null);
        tv_merchant_name = rootView.findViewById(R.id.tv_merchant_name);
        tv_merchant_name.setText(mch_name);

        tv_merchant_id = rootView.findViewById(R.id.tv_merchant_id);
        tv_merchant_id.setText(mch_id);

        tv_speaker_id = rootView.findViewById(R.id.tv_speaker_id);
        tv_speaker_id.setText(speaker_id);

        btn_cancel = rootView.findViewById(R.id.btn_cancel);
        btn_confirm = rootView.findViewById(R.id.btn_confirm);
        setContentView(rootView);

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        btn_confirm.setOnClickListener(confirmButtonListener);
    }

}
