package cn.swiftpass.enterprise.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import cn.swiftpass.enterprise.bussiness.model.TipsBean;
import cn.swiftpass.enterprise.intl.R;

/**
 * Created by aijingya on 2019/7/2.
 *
 * @Package cn.swiftpass.enterprise.ui.adapter
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2019/7/2.15:47.
 */
public class TipsRecycleviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<TipsBean> TipsAdapterBeanList = new ArrayList<>();

    public TipsRecycleviewAdapter(Context context,List<TipsBean> datas){
        this.mContext = context;
        TipsAdapterBeanList = datas;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(mContext).inflate(R.layout.item_tips_layout, parent,false);
        return new MyViewHolderTips(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final MyViewHolderTips Myholder = (MyViewHolderTips) holder;

        if(!TextUtils.isEmpty(TipsAdapterBeanList.get(position).getLabel())){
            Myholder.tv_label.setText(TipsAdapterBeanList.get(position).getLabel());
        }
        if(TipsAdapterBeanList.get(position).isDefaultTip()){
            Myholder.tv_default_label.setVisibility(View.VISIBLE);
        }else{
            Myholder.tv_default_label.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(TipsAdapterBeanList.get(position).getTipsRate())){
            Myholder.tv_proprotion.setText(TipsAdapterBeanList.get(position).getTipsRate()+"%");
        }

    }

    @Override
    public int getItemCount() {
        return TipsAdapterBeanList.size();
    }

    public class MyViewHolderTips extends RecyclerView.ViewHolder {
        TextView tv_label;              //选项标签
        TextView tv_default_label;     //是否展示default标签
        TextView tv_proprotion;        //比例

        public MyViewHolderTips(View view) {
            super(view);
            tv_label = (TextView) view.findViewById(R.id.tv_label);
            tv_default_label = (TextView) view.findViewById(R.id.tv_default_label);
            tv_proprotion = (TextView) view.findViewById(R.id.tv_proprotion);
        }
    }
}
