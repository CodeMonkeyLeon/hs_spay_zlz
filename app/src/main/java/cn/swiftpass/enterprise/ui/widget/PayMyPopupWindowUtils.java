package cn.swiftpass.enterprise.ui.widget;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import cn.swiftpass.enterprise.intl.R;

public class PayMyPopupWindowUtils implements OnClickListener
{
    
    private OnPopuWindowItemClickListener listener;
    
    private Context context;
    
    public interface OnPopuWindowItemClickListener
    {
        void onPopuWindowItemClick(View v);
    }
    
    public PayMyPopupWindowUtils(Context context, OnPopuWindowItemClickListener listener)
    {
        this.context = context;
        this.listener = listener;
    }
    
    @Override
    public void onClick(View v)
    {
        if (listener != null)
        {
            listener.onPopuWindowItemClick(v);
        }
    }
    
    public String PAY_QQ_NATIVE1 = "pay.qq.jspay"; // 手Q扫码
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public PopupWindow getPayHelpDialog(String payType, int width)
    {
        View v = View.inflate(context, R.layout.micropay_help_layout, null);
        final PopupWindow pop = new PopupWindow(v, width, ViewGroup.LayoutParams.MATCH_PARENT);
        ImageView micropay_help_img = (ImageView)v.findViewById(R.id.micropay_help_img);
        //支付帮助
        //        if (payType.equals(MainApplication.QQ_SACN_TYPE) || payType.equalsIgnoreCase(PAY_QQ_NATIVE1))
        //        {
        //            //手Q
        //            micropay_help_img.setImageResource(R.drawable.micropay_help_qq);
        //        }
        //        else if (payType.equals(MainApplication.ZFB_SCAN_TYPE) || payType.equals(MainApplication.ZFB_SCAN_TYPE2))
        //        {
        //            //支付宝扫码
        //            micropay_help_img.setImageResource(R.drawable.micropay_help_zfb);
        //        }
        //        else
        //        {
        //            //微信扫码
        //            micropay_help_img.setImageResource(R.drawable.micropay_help_weixin);
        //        }
        micropay_help_img.setOnClickListener(new OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                pop.dismiss();
            }
        });
        pop.setWidth(width);
        pop.setAnimationStyle(android.R.style.Animation_Dialog);
        pop.update();
        // 设置PopupWindow外部区域是否可触摸
        pop.setBackgroundDrawable(context.getResources().getDrawable(android.R.color.transparent));
        pop.setFocusable(true); // 设置PopupWindow可获得焦点
        pop.setTouchable(true); // 设置PopupWindow可触摸
        pop.setOutsideTouchable(false); // 设置非PopupWindow区域可触摸
        return pop;
    }
}
