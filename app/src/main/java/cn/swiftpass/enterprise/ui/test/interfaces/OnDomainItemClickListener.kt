package cn.swiftpass.enterprise.ui.test.interfaces

import android.view.View
import cn.swiftpass.enterprise.ui.test.entity.DomainEntity

interface OnDomainItemClickListener {
    fun onItemClick(position: Int, entity: DomainEntity, view: View)
}