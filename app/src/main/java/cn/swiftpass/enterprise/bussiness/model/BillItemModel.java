package cn.swiftpass.enterprise.bussiness.model;

public class BillItemModel
{
    private long payNetFee;//支付净金额
    
    private long mchFee = 0;//手续费
    
    private long payRemitFee; // 划账金额
    
    private long payTradeTime;
    
    private String payTypaName;
    
    /**
     * @return 返回 payTypaName
     */
    public String getPayTypaName()
    {
        return payTypaName;
    }
    
    /**
     * @param 对payTypaName进行赋值
     */
    public void setPayTypaName(String payTypaName)
    {
        this.payTypaName = payTypaName;
    }
    
    /**
     * @return 返回 payNetFee
     */
    public long getPayNetFee()
    {
        return payNetFee;
    }
    
    /**
     * @param 对payNetFee进行赋值
     */
    public void setPayNetFee(long payNetFee)
    {
        this.payNetFee = payNetFee;
    }
    
    /**
     * @return 返回 mchFee
     */
    public long getMchFee()
    {
        return mchFee;
    }
    
    /**
     * @param 对mchFee进行赋值
     */
    public void setMchFee(long mchFee)
    {
        this.mchFee = mchFee;
    }
    
    /**
     * @return 返回 payRemitFee
     */
    public long getPayRemitFee()
    {
        return payRemitFee;
    }
    
    /**
     * @param 对payRemitFee进行赋值
     */
    public void setPayRemitFee(long payRemitFee)
    {
        this.payRemitFee = payRemitFee;
    }
    
    /**
     * @return 返回 payTradeTime
     */
    public long getPayTradeTime()
    {
        return payTradeTime;
    }
    
    /**
     * @param 对payTradeTime进行赋值
     */
    public void setPayTradeTime(long payTradeTime)
    {
        this.payTradeTime = payTradeTime;
    }
    
}
