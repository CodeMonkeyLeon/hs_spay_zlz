package cn.swiftpass.enterprise.bussiness.enums;

/**
 * 扫描状态
 * User: Administrator
 * Date: 13-11-7
 * Time: 下午1:06
 * To change this template use File | Settings | File Templates.
 */
public enum QRCodeState {

    DEFAULT(0,"请使用微信扫一扫完成支付。"),
    NO_PAY(408,"请使用微信扫一扫完成支付。"),
    PAY_N(200,"请使用微信扫一扫完成支付。"),//未知状态
    SCAN_SUCCESS(203,"扫描成功,请在手机确认支付。"),
    SCAN_SUCCESS_CANCEL(205,"用户取消扫描。"),
    PAY(204,"支付未知。"),
    PAY_SUCCESS(201,"交易成功。"),
    PAY_FAIL(202,"支付失败。"),
    FAIL_UUID(400,"uuid失效。");


    private final Integer value;
    private String displayName = "";

    private QRCodeState(Integer v ,String displayName)
    {
        this.value = v;
        this.displayName = displayName;

    }
    public static String getDisplayNameByVaue(Integer v){
        if(v!=null){
            for (QRCodeState ose : values()) {
                if (ose.value.equals(v)){
                    return ose.getDisplayName();
                }
            }
        }
        return "";
    }
    public String getDisplayName() {
        return displayName;
    }

    public Integer getValue() {
        return value;
    }



}
