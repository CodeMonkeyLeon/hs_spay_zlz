package cn.swiftpass.enterprise.bussiness.logica.upgrade;

import android.content.SharedPreferences;

import org.json.JSONObject;

import java.util.HashMap;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.BaseManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.NotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.MD5;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.Utils;

/**
 * User: Alan
 * Date: 13-9-21
 * Time: 下午9:03
 */
public class UpgradeManager extends BaseManager {
    private SharedPreferences sp;

    @Override
    public void init() {
    }

    private UpgradeManager() {
        sp = MainApplication.getContext().getApplicationPreferences();
    }

    @Override
    public void destory() {

    }

    private static UpgradeManager instance;

    public static UpgradeManager getInstance() {
        if (instance == null) {
            instance = new UpgradeManager();
        }
        return instance;
    }

    /**
     * 获取服务器版本
     */
    public void getVersonCode(final NotifyListener<UpgradeInfo> listener) {
        ThreadHelper.executeWithCallback(new Executable<UpgradeInfo>() {
            @Override
            public UpgradeInfo execute() throws Exception {
                JSONObject param = new JSONObject();
                HashMap<String, String> hashMap = new HashMap<String, String>();

                param.put("verCode", AppHelper.getVerCode(MainApplication.getContext()));
                param.put("bankCode", ApiConstant.bankCode);
                hashMap.put("verCode", Integer.toString(AppHelper.getVerCode(MainApplication.getContext())));
                hashMap.put("bankCode", ApiConstant.bankCode);

//                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
//                    param.put("sign", SignUtil.getInstance().createSign(hashMap, MainApplication.getSignKey()));
//                }

                long spayRs = System.currentTimeMillis();
                //加签名
                String priKey = (String) SharedPreUtile.readProduct("secretKey");
                param.put("spayRs", String.valueOf(spayRs));

                if (!StringUtil.isEmptyOrNull(MainApplication.skey)) {
                    param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MD5.md5s(priKey).substring(0, 16)));

                } else {
                    param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MD5.md5s(String.valueOf(spayRs))));
                }

                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.UPGRADE_CHECKIN, param, String.valueOf(spayRs), null);
                UpgradeInfo info;
                if (result.data == null) {
                    return null;
                }
                Integer rse = Integer.parseInt(result.data.getString("result"));
                //                //测试账号配置信息
                switch (rse) {
                    case 200:
                        String upgradeinfo = result.data.getString("message");
                        info = new UpgradeInfo();
                        JSONObject json = new JSONObject(upgradeinfo);
                        info.dateTime = Utils.Long.tryParse(json.optString("updated_at"), 0);
                        info.message = json.optString("subDesc", "");
                        info.version = Utils.Integer.tryParse(json.optString("verCode"), 0);
                        info.mustUpgrade = Utils.Integer.tryParse(json.optString("isUpdate"), 0) == 0 ? true : false;
                        info.versionName = json.optString("verName");
                        info.fileMd5 = json.optString("fileMd5");
                        MainApplication.fileMd5 = info.fileMd5;
                        info.url = json.optString("filePath", "");
                        return info;
                    case 400:
                        listener.onSucceed(null);
                        return null;
                    case 402:
                        listener.onSucceed(null);
                        return null;
                    default:
                        listener.onError(rse);
                        return null;
                }

            }
        }, listener);
    }

}
