package cn.swiftpass.enterprise.io.database.table;

import android.database.sqlite.SQLiteDatabase;

/**
 * 店铺管理表
 * User: Administrator
 * Date: 13-12-23
 * Time: 下午2:38
 * To change this template use File | Settings | File Templates.
 */
public class ShopTable extends TableBase{
    public static final String TABLE_NAME="tb_shop";
    public static final String COLUMN_SID="shop_id";
    public static final String COLUMN__MID="m_id";
    public static final String COLUMN__NAME="name";
    public static final String COLUMN__CITY="city";
    public static final String COLUMN__SHOP_TYPE="shop_type";
    public static final String COLUMN__SHOP_AMOUN="shop_amount";
    public static final String COLUMN__ADDRESS="address";
    public static final String COLUMN__LINK_PHONE="link_phone";
    public static final String COLUMN__START_TIME="start_time";
    public static final String COLUMN__STR_START_TIME="str_start_time";
    public static final String COLUMN__END_TIME="end_time";
    public static final String COLUMN__STR_END_TIME="str_end_time";

    public static final String COLUMN__TRAFFIC_INFO="traffic_info";
    public static final String COLUMN__ATTACH_INFO="attach_info";
    public static final String COLUMN__REMARK="remark";
    public static final String COLUMN_PIC_PATH="picPath";
    public static final String COLUMN_UID="uId";
    public static final String COLUMN_ADD_TIME="add_time";
    @Override
    public void createTable(SQLiteDatabase db) {
        final String SQL_CREATE_TABLE = "create table "+TABLE_NAME+"("
                + COLUMN_ID + " INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT  ,"
                +COLUMN_SID + " INT ,"
                +COLUMN__MID + " INT ,"
                +COLUMN__NAME + " VARCHAR(50) ,"
                +COLUMN__CITY + " VARCHAR(20) ,"
                +COLUMN_PIC_PATH + " VARCHAR(60) ,"
                +COLUMN__SHOP_TYPE + " INT ,"
                +COLUMN__SHOP_AMOUN + " INT ,"
                +COLUMN__ADDRESS + " VARCHAR(50) ,"
                +COLUMN__LINK_PHONE + " VARCHAR(50) ,"
                +COLUMN__START_TIME + " Time ,"
                +COLUMN__TRAFFIC_INFO + " VARCHAR(50) ,"
                +COLUMN__END_TIME + " Time ,"
                +COLUMN__STR_START_TIME + " VARCHAR(50) ,"
                +COLUMN__STR_END_TIME + " VARCHAR(50) ,"
                +COLUMN__ATTACH_INFO + " VARCHAR(50) ,"
                +COLUMN_UID + " LONG ,"
                +COLUMN_ADD_TIME + " LONG ,"
                +COLUMN__REMARK + " VARCHAR(100)"

                +")";

        db.execSQL(SQL_CREATE_TABLE);
    }
}
