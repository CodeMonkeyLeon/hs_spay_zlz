package cn.swiftpass.enterprise.utils

import android.os.Build
import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.intl.BuildConfig
import com.example.printsdk.Constants
import com.google.gson.Gson
import java.util.*
import kotlin.random.Random

object KotlinUtils {


    /**
     * Kotlin匿名函数
     *
     * 获取6位小数
     * 不足6位补零，大于6位取前六位，无四舍五入
     */
    val retainSixDecimalPlaces: (String?) -> String? = { num ->
        var result = num
        try {
            num?.run {
                val array = this.split(".")
                if (array.size < 2) {
                    //不含小数部分, 如1, 则转换为1.000000
                    result = "${array[0]}.000000}"
                } else {
                    //包含小数部分
                    val intPart = array[0]
                    val decimalPart = array[1]
                    if (decimalPart.length == 6) {
                        result = "$intPart.$decimalPart"
                    } else if (decimalPart.length > 6) {
                        result = "$intPart.${decimalPart.substring(0, 6)}"
                    } else {
                        val zeroCnt = 6 - decimalPart.length
                        var zero = ""
                        for (i in 1..zeroCnt) {
                            zero += "0"
                        }
                        result = "$intPart.$decimalPart$zero"
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        result
    }


    fun isToday(selectTime: String): Boolean {
        val newDate = DateUtil.formatYYMD(System.currentTimeMillis())
        val arr1 = newDate.split("-").toTypedArray()
        val arr2 = selectTime.split("-").toTypedArray()
        return arr1[0] == arr2[0] && arr1[1] == arr2[1] && arr1[2] == arr2[2]
    }


    fun isAbsoluteNullStr(s: String): Boolean {
        val str = deleteBlank(s)
        return str == null || str.isEmpty() || "null".equals(str, ignoreCase = true)

    }


    /**
     * 获取1...10之间的随机数
     */
    fun getRandomNumberOneToTen() = Random.nextInt(10) + 1


    /**
     * 去前后空格和去换行符
     */
    private fun deleteBlank(str: String): String {
        return if (null != str && str.trim { it <= ' ' }.isNotEmpty()) {
            val array = str.toCharArray()
            var start = 0
            var end = array.size - 1
            while (array[start] == ' ') start++
            while (array[end] == ' ') end--
            str.substring(start, end + 1).replace("\n".toRegex(), "")
        } else {
            ""
        }
    }


    /**
     * 是否是 POS机
     */
    fun isPOS() = MainApplication.IS_POS_VERSION


    /**
     * 是否是 A8
     */
    fun isA8Print(): Boolean {
        return MainApplication.IS_POS_VERSION && TextUtils.equals(
            Build.MODEL,
            Constants.TYPE_A8
        )
    }


    /**
     * 打印测试数据
     */
    fun getOrderTestData(): Order {
        val dataStr =
            "{\"addTime\":\"1652926252842\",\"addTimeNew\":\"2022-05-19 10:10:54\",\"add_time\":1652926252842,\"affirm\":0,\"apiCode\":\"1\",\"attach\":\"\",\"authNo\":\"\",\"body\":\"Mobile payment\",\"canAffim\":0,\"canAffirm\":0,\"cashFeel\":1,\"client\":\"SPAY_POS\",\"clientType\":0,\"costFee\":0,\"createOrderType\":0,\"createTime\":0,\"daMoney\":0,\"employeeId\":0,\"finishTime\":0,\"id\":0,\"isAgainPay\":0,\"isMark\":false,\"isPay\":true,\"mchName\":\"Vendor Zero\",\"money\":2,\"note\":\"\",\"notifyTime\":\"1652926254000\",\"notify_time\":0,\"operateTime\":\"\",\"orderFee\":2,\"orderNo\":\"\",\"orderNoMch\":\"129590000979202205191199967725\",\"outAuthNo\":\"\",\"outTradeNo\":\"129590000979146965771286316086\",\"pageCount\":0,\"partner\":\"顧客存根\",\"payType\":0,\"refundFee\":0,\"refundFeel\":0,\"refundMoney\":0,\"refundState\":0,\"requestNo\":\"\",\"restAmount\":0,\"rfMoneyIng\":0,\"rufundMark\":1,\"surcharge\":0,\"tipFee\":0,\"totalFee\":2,\"totalFreezeAmount\":0,\"totalPayAmount\":0,\"totalRows\":0,\"totalUnfreezeAmount\":0,\"tradeName\":\"WeChat-Quick Pay\",\"tradeState\":2,\"tradeStateText\":\"\",\"tradeTime\":\"1652926254000\",\"tradeTimeNew\":\"2022-05-19 10:10:54\",\"tradeType\":\"pay.weixin.proxy.micropay.intl\",\"transactionId\":\"4200001420202205195498101689\",\"transactionType\":0,\"uid\":0,\"unsettledDiscountFee\":0,\"useId\":\"\",\"useTime\":0,\"userName\":\"Vendor Zero\",\"vat\":0,\"withholdingTax\":0}"
        return stringToJson(dataStr, Order()) as Order
    }


    /**
     * json对象 -> String
     */
    fun <T> jsonToString(t: T): String {
        val gson = Gson()
        return gson.toJson(t)
    }

    /**
     * String -> json对象
     */
    fun <T> stringToJson(str: String, t: T): T? {
        try {
            val gson = Gson()
            t?.let {
                return gson.fromJson(str, it::class.java) as T
            }
            return null
        } catch (e: Exception) {
            if (BuildConfig.isDebug) {
                e.printStackTrace()
            }
        }
        return null
    }


    /**
     * 获取系统当前语言类型
     */
    fun getLanguageType(): String {
        val language = PreferenceUtil.getString("language", "")
        return if (TextUtils.isEmpty(language)) {
            val lan = Locale.getDefault().toString()
            if (lan.equals(MainApplication.LANG_CODE_ZH_CN, ignoreCase = true)
                || lan.equals(
                    MainApplication.LANG_CODE_ZH_CN_HANS, ignoreCase = true
                )
            ) {
                //简体中文
                MainApplication.LANG_CODE_ZH_CN
            } else if (lan.equals(MainApplication.LANG_CODE_ZH_HK, ignoreCase = true)
                || lan.equals(
                    MainApplication.LANG_CODE_ZH_MO, ignoreCase = true
                ) || lan.equals(MainApplication.LANG_CODE_ZH_TW, ignoreCase = true) || lan.equals(
                    MainApplication.LANG_CODE_ZH_HK_HANT,
                    ignoreCase = true
                )
            ) {
                //繁体中文
                MainApplication.LANG_CODE_ZH_TW
            } else {
                //默认英文
                MainApplication.LANG_CODE_EN_US
            }
        } else when (language) {
            MainApplication.LANG_CODE_ZH_CN -> MainApplication.LANG_CODE_ZH_CN
            MainApplication.LANG_CODE_ZH_TW -> MainApplication.LANG_CODE_ZH_TW
            else -> MainApplication.LANG_CODE_EN_US
        }
    }


}