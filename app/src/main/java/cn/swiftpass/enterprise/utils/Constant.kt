package cn.swiftpass.enterprise.utils

object Constant {

    const val SERVER_CONFIG = "serverCifg"


    const val SERVER_ADDRESS_DEFAULT = "default"
    const val SERVER_ADDRESS_SELF = "self"
    const val SERVER_ADDRESS_PRD = "prd"
    const val SERVER_ADDRESS_DEV = "dev"
    const val SERVER_ADDRESS_DEV_JH = "dev-jh"
    const val SERVER_ADDRESS_TEST_123 = "test123"
    const val SERVER_ADDRESS_TEST_61 = "test61"
    const val SERVER_ADDRESS_TEST_63 = "test63"
    const val SERVER_ADDRESS_TEST_UAT = "testUAT"
    const val SERVER_ADDRESS_OVERSEAS_61 = "overseas61"
    const val SERVER_ADDRESS_OVERSEAS_63 = "overseas63"
    const val SERVER_ADDRESS_CHECKOUT_DEV = "checkoutDev"
    const val RATE_CONFIRM_INFO = "RATE_CONFIRM_INFO"
}