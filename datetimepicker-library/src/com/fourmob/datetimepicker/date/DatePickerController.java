package com.fourmob.datetimepicker.date;

import com.fourmob.datetimepicker.test.MainTest;

public abstract interface DatePickerController
{
    public abstract int getFirstDayOfWeek();
    
    public abstract int getMaxYear();
    
    public abstract int getMinYear();
    
    public abstract SimpleMonthAdapter.CalendarDay getSelectedDay();
    
    public abstract void onDayOfMonthSelected(int year, int month, int day);
    
    public abstract void onYearSelected(int year);
    
    public abstract void registerOnDateChangedListener(DatePickerDialog.OnDateChangedListener onDateChangedListener);
    
    public abstract void tryVibrate();
    
    public abstract void registerOnDateChangedListener(MainTest.OnDateChangedListener onDateChangedListener);
    
    //得到 当前年
    public abstract int getCurrentYear();
    
    //得到 当前月
    public abstract int getCurrentMonth();
}