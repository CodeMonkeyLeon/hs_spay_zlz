package com.example.printsdk.A920

import android.content.Context
import com.pax.dal.IDAL
import com.pax.neptunelite.api.DALProxyClient

class POSA920Client {


    companion object {
        const val DELAY_TIME = 3000L
        var idal: IDAL? = null
        private var sA920Client: POSA920Client? = null
        fun initA920(context: Context) {
            if (sA920Client == null) {
                sA920Client = POSA920Client(context)
            }
        }
    }

    private var mContext: Context? = null

    fun getInstance(): POSA920Client? {
        if (sA920Client == null) {
            throw RuntimeException("A920Client is not initialized")
        }
        return sA920Client
    }


    constructor(context: Context) {
        mContext = context
        //百富pos初始化
        try {
            idal = DALProxyClient.getInstance().getDal(mContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}