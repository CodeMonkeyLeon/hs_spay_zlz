package com.example.printsdk.A920.printUtils

import android.graphics.Bitmap
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.google.zxing.common.BitMatrix
import java.util.*

object CreateOneDiCodeUtilPrint {
    private const val CODE = "utf-8"

    fun createCode(str: String?, width: Int?, height: Int?): Bitmap? {
        var width = width
        var height = height
        if (width == null || width < 260) {
            width = 260
        }
        if (height == null || height < 260) {
            height = 260
        }
        try {
            // 文字编码
            val hints = Hashtable<EncodeHintType, String?>()
            hints[EncodeHintType.CHARACTER_SET] = CODE
            val bitMatrix =
                MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, width, height, hints)
            return BitMatrixToBitmap(bitMatrix)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    /**
     * BitMatrix转换成Bitmap
     * @param matrix
     * @return
     */
    private fun BitMatrixToBitmap(matrix: BitMatrix): Bitmap? {
        val WHITE = -0x1
        val BLACK = -0x1000000
        val width = matrix.width
        val height = matrix.height
        val pixels = IntArray(width * height)
        for (y in 0 until height) {
            val offset = y * width
            for (x in 0 until width) {
                pixels[offset + x] = if (matrix[x, y]) BLACK else WHITE
            }
        }
        return createBitmap(width, height, pixels)
    }

    /**
     * 生成Bitmap
     * @param width
     * @param height
     * @param pixels
     * @return
     */
    private fun createBitmap(width: Int, height: Int, pixels: IntArray): Bitmap? {
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
        return bitmap
    }
}