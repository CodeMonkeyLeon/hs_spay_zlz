package org.xclcharts.common;

/**
 * @InterfaceName IFormatterDoubleCallBack
 * @Description  用于Double类型的回调接口
 * @author XiongChuanLiang<br/>(xcl_168@aliyun.com)
 *  * MODIFIED    YYYY-MM-DD   REASON
 */

public interface IFormatterDoubleCallBack {
	
	public String doubleFormatter(Double value);
	
	
}
