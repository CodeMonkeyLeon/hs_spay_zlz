package org.xclcharts.renderer.info;


/**
 * @ClassName PlotAxisTick
 * @Description  用于记录轴刻度的位置信息
 * @author XiongChuanLiang<br/>(xcl_168@aliyun.com)
 *  
 */
public class PlotAxisTick extends PlotDataInfo{
	
	private boolean mShowTickMarks = true;
	
	public PlotAxisTick(){}
	
	
	public PlotAxisTick(float x,float y,String label)
	{
		 X = x;
		 Y = y;
		 Label = label;
		 labelX = x;
		 labelY = y;
	};
	
	public PlotAxisTick(int id,float x,float y,String label)
	{
		 ID = id;
		 X = x;
		 Y = y;
		 Label = label;
		 
		 labelX = x;
		 labelY = y;
	}
	
	public PlotAxisTick(float x,float y,String label,float lx,float ly)
	{
		// ID = id;
		 X = x;
		 Y = y;
		 Label = label;
		 
		 labelX = lx;
		 labelY = ly;
	}
	
	public PlotAxisTick(float x,float y,String label,float lx,float ly,boolean tickMarks)
	{
		// ID = id;
		 X = x;
		 Y = y;
		 Label = label;
		 
		 labelX = lx;
		 labelY = ly;
		 
		 mShowTickMarks = tickMarks;
	}
	
	
	
	public float getLabelX() 
	{
		return labelX;
	}

	public void setLabelX(float x) 
	{
		labelX = x;
	}

	public float getLabelY() 
	{
		return labelY;
	}

	public void setLabelY(float y) 
	{
		labelY = y;
	}
	
	public boolean isShowTickMarks()
	{
		return mShowTickMarks;
	}

}
