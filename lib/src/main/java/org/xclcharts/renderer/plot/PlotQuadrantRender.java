package org.xclcharts.renderer.plot;

import android.graphics.Canvas;

/**
 * @ClassName PlotQuadrantRender
 * @Description 四象限绘制类
 * @author XiongChuanLiang<br/>(xcl_168@aliyun.com)
 *
 */
public class PlotQuadrantRender extends PlotQuadrant{
	
	public PlotQuadrantRender(){}
	
	public void drawQuadrant(Canvas canvas,
			float centerX,float centerY,
			float left,float top,float right,float bottom)
	{
		
		if(mShowBgColor) 	//绘制bg
		{		
			getBgColorPaint().setColor(mFirstColor);
			canvas.drawRect(centerX, top, right, centerY,  getBgColorPaint());
			
			getBgColorPaint().setColor(mSecondColor);
			canvas.drawRect( centerX, centerY, right, bottom, getBgColorPaint());
			
			getBgColorPaint().setColor(mThirdColor);
			canvas.drawRect(left, centerY,centerX ,bottom ,  getBgColorPaint());
			
			getBgColorPaint().setColor(mFourthColor);
			canvas.drawRect(left, top,centerX, centerY,  getBgColorPaint());
		}
		
		if(mShowVerticalLine)
		{
			canvas.drawLine(centerX, top, centerX, bottom,  getVerticalLinePaint());
		}
		
		if(mShowHorizontalLine)
		{
			canvas.drawLine(left, centerY, right, centerY,  getVerticalLinePaint());
		}				
	}
	

}
