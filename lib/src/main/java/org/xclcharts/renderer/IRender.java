package org.xclcharts.renderer;

/**
 * @InterfaceName IRender
 * @Description  用于绘制的接口
 * @author XiongChuanLiang<br/>(xcl_168@aliyun.com)
 */

import android.graphics.Canvas;

public interface IRender {	
	public boolean render(Canvas canvas) throws Exception;
}
